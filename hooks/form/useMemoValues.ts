import { useMemo } from "react";
import { FormErrorObject, FormTouchedObject } from "./type";
import {
  getFormHasVisibleErrorObject,
  getFormStore,
  getFormValidity,
  getFormValidityObject,
} from "./utility";

export const useMemoValues = <FormValueObject>(
  formValueObject: FormValueObject,
  formErrorObject: FormErrorObject<FormValueObject>,
  formTouchedObject: FormTouchedObject<FormValueObject>
) => {
  const formValidityObject = useMemo(() => {
    return getFormValidityObject<FormValueObject>(formErrorObject);
  }, [formErrorObject]);

  const formHasVisibleErrorObject = useMemo(() => {
    return getFormHasVisibleErrorObject<FormValueObject>(
      formValidityObject,
      formTouchedObject
    );
  }, [formTouchedObject, formValidityObject]);

  const formStore = useMemo(() => {
    return getFormStore<FormValueObject>(
      formValueObject,
      formErrorObject,
      formHasVisibleErrorObject
    );
  }, [formErrorObject, formHasVisibleErrorObject, formValueObject]);

  const formValidity = useMemo(() => {
    return getFormValidity<FormValueObject>(formValidityObject);
  }, [formValidityObject]);

  return {
    formStore,
    formValidity,
  };
};
