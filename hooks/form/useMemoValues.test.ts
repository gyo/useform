import { renderHook } from "@testing-library/react-hooks";
import { useMemoValues } from "./useMemoValues";

describe("useMemoValues", () => {
  test("form の要素が空", () => {
    const hook = renderHook(() => useMemoValues({}, {}, {}));

    expect(hook.result.current.formStore).toEqual({});
    expect(hook.result.current.formValidity).toBe(true);
  });

  describe("form の要素が一つ", () => {
    describe("Error が存在しない", () => {
      test("Touched が false", () => {
        const hook = renderHook(() =>
          useMemoValues({ name: "a" }, { name: [] }, { name: false })
        );

        expect(hook.result.current.formStore).toEqual({
          name: { value: "a", error: [], hasVisibleError: false },
        });
        expect(hook.result.current.formValidity).toBe(true);
      });

      test("Touched が true", () => {
        const hook = renderHook(() =>
          useMemoValues({ name: "a" }, { name: [] }, { name: true })
        );

        expect(hook.result.current.formStore).toEqual({
          name: { value: "a", error: [], hasVisibleError: false },
        });
        expect(hook.result.current.formValidity).toBe(true);
      });
    });

    describe("Error が存在する", () => {
      test("Touched が false", () => {
        const hook = renderHook(() =>
          useMemoValues({ name: "a" }, { name: ["A"] }, { name: false })
        );

        expect(hook.result.current.formStore).toEqual({
          name: { value: "a", error: ["A"], hasVisibleError: false },
        });
        expect(hook.result.current.formValidity).toBe(false);
      });

      test("Touched が true", () => {
        const hook = renderHook(() =>
          useMemoValues({ name: "a" }, { name: ["A"] }, { name: true })
        );

        expect(hook.result.current.formStore).toEqual({
          name: { value: "a", error: ["A"], hasVisibleError: true },
        });
        expect(hook.result.current.formValidity).toBe(false);
      });
    });
  });

  describe("form の要素が複数", () => {
    describe("すべての Error が存在しない", () => {
      test("すべての Touched が false", () => {
        const hook = renderHook(() =>
          useMemoValues(
            { name: "a", age: 1, items: ["1", "2", "3"] },
            { name: [], age: [], items: [] },
            { name: false, age: false, items: false }
          )
        );

        expect(hook.result.current.formStore).toEqual({
          name: { value: "a", error: [], hasVisibleError: false },
          age: { value: 1, error: [], hasVisibleError: false },
          items: { value: ["1", "2", "3"], error: [], hasVisibleError: false },
        });
        expect(hook.result.current.formValidity).toBe(true);
      });

      test("いくつかの Touched が true", () => {
        const hook = renderHook(() =>
          useMemoValues(
            { name: "a", age: 1, items: ["1", "2", "3"] },
            { name: [], age: [], items: [] },
            { name: true, age: false, items: true }
          )
        );

        expect(hook.result.current.formStore).toEqual({
          name: { value: "a", error: [], hasVisibleError: false },
          age: { value: 1, error: [], hasVisibleError: false },
          items: { value: ["1", "2", "3"], error: [], hasVisibleError: false },
        });
        expect(hook.result.current.formValidity).toBe(true);
      });

      test("すべての Touched が true", () => {
        const hook = renderHook(() =>
          useMemoValues(
            { name: "a", age: 1, items: ["1", "2", "3"] },
            { name: [], age: [], items: [] },
            { name: true, age: true, items: true }
          )
        );

        expect(hook.result.current.formStore).toEqual({
          name: { value: "a", error: [], hasVisibleError: false },
          age: { value: 1, error: [], hasVisibleError: false },
          items: { value: ["1", "2", "3"], error: [], hasVisibleError: false },
        });
        expect(hook.result.current.formValidity).toBe(true);
      });
    });

    describe("いくつかの Error が存在する", () => {
      test("すべての Touched が false", () => {
        const hook = renderHook(() =>
          useMemoValues(
            { name: "a", age: 1, items: ["1", "2", "3"] },
            { name: ["A"], age: ["B"], items: [] },
            { name: false, age: false, items: false }
          )
        );

        expect(hook.result.current.formStore).toEqual({
          name: { value: "a", error: ["A"], hasVisibleError: false },
          age: { value: 1, error: ["B"], hasVisibleError: false },
          items: { value: ["1", "2", "3"], error: [], hasVisibleError: false },
        });
        expect(hook.result.current.formValidity).toBe(false);
      });

      test("いくつかの Touched が true", () => {
        const hook = renderHook(() =>
          useMemoValues(
            { name: "a", age: 1, items: ["1", "2", "3"] },
            { name: ["A"], age: ["B"], items: [] },
            { name: true, age: false, items: true }
          )
        );

        expect(hook.result.current.formStore).toEqual({
          name: { value: "a", error: ["A"], hasVisibleError: true },
          age: { value: 1, error: ["B"], hasVisibleError: false },
          items: { value: ["1", "2", "3"], error: [], hasVisibleError: false },
        });
        expect(hook.result.current.formValidity).toBe(false);
      });

      test("すべての Touched が true", () => {
        const hook = renderHook(() =>
          useMemoValues(
            { name: "a", age: 1, items: ["1", "2", "3"] },
            { name: ["A"], age: ["B"], items: [] },
            { name: true, age: true, items: true }
          )
        );

        expect(hook.result.current.formStore).toEqual({
          name: { value: "a", error: ["A"], hasVisibleError: true },
          age: { value: 1, error: ["B"], hasVisibleError: true },
          items: { value: ["1", "2", "3"], error: [], hasVisibleError: false },
        });
        expect(hook.result.current.formValidity).toBe(false);
      });
    });
  });
});
