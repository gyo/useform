import { Dispatch, SetStateAction, useCallback } from "react";
import { FormTouchedObject, OnBlur } from "./type";

export const useOnBlur = <FormValueObject>(
  formTouchedObject: FormTouchedObject<FormValueObject>,
  setFormTouchedObject: Dispatch<
    SetStateAction<FormTouchedObject<FormValueObject>>
  >
) => {
  const onBlur: OnBlur<FormValueObject> = useCallback(
    (name) => () => {
      const nextFormTouchedObject = { ...formTouchedObject, [name]: true };

      setFormTouchedObject(nextFormTouchedObject);
    },
    [formTouchedObject, setFormTouchedObject]
  );

  return {
    onBlur,
  };
};
