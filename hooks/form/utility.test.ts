import {
  FormErrorObject,
  FormHasVisibleErrorObject,
  FormTouchedObject,
  FormValidityObject,
  RelationNamesObject,
  ValidatorObject,
} from "./type";
import {
  getFormErrorObject,
  getFormHasVisibleErrorObject,
  getFormStore,
  getFormValidity,
  getFormValidityObject,
  getNextFormErrorObject,
  refreshFormTouchedObject,
} from "./utility";

describe("getNextFormErrorObject", () => {
  type V = {
    text: string;
    number: number | "";
    multiItems: string[];
    relation0: string;
    relation1: string;
  };
  const before: FormErrorObject<V> = {
    text: [],
    number: [],
    multiItems: [],
    relation0: [],
    relation1: [],
  };
  const formValueObject: V = {
    text: "",
    number: "",
    multiItems: [],
    relation0: "",
    relation1: "",
  };
  const validatorObject: ValidatorObject<V> = {
    text: () => ["検査済み"],
    number: () => ["検査済み"],
    multiItems: () => ["検査済み"],
    relation0: () => ["検査済み"],
    relation1: () => ["検査済み"],
  };
  const relationNamesObject: RelationNamesObject<V> = {
    relation0: ["relation0", "relation1"],
  };

  test("check text", () => {
    const after = getNextFormErrorObject(
      before,
      ["text"],
      formValueObject,
      validatorObject,
      relationNamesObject
    );
    expect(after).toEqual({
      ...before,
      text: ["検査済み"],
    });
  });

  test("check text without validatorObject", () => {
    const after = getNextFormErrorObject(
      before,
      ["text"],
      formValueObject,
      undefined,
      relationNamesObject
    );
    expect(after).toEqual({
      ...before,
    });
  });

  test("check number", () => {
    const after = getNextFormErrorObject(
      before,
      ["number"],
      formValueObject,
      validatorObject,
      relationNamesObject
    );
    expect(after).toEqual({
      ...before,
      number: ["検査済み"],
    });
  });

  test("check multiItems", () => {
    const after = getNextFormErrorObject(
      before,
      ["multiItems"],
      formValueObject,
      validatorObject,
      relationNamesObject
    );
    expect(after).toEqual({
      ...before,
      multiItems: ["検査済み"],
    });
  });

  test("check relation0", () => {
    const after = getNextFormErrorObject(
      before,
      ["relation0"],
      formValueObject,
      validatorObject,
      relationNamesObject
    );
    expect(after).toEqual({
      ...before,
      relation0: ["検査済み"],
      relation1: ["検査済み"],
    });
  });

  test("check relation0 without relationNamesObject", () => {
    const after = getNextFormErrorObject(
      before,
      ["relation0"],
      formValueObject,
      validatorObject,
      undefined
    );
    expect(after).toEqual({
      ...before,
      relation0: ["検査済み"],
    });
  });

  test("check invalid", () => {
    const after = getNextFormErrorObject(
      before,
      (["invalid"] as unknown) as (keyof V)[],
      formValueObject,
      validatorObject,
      undefined
    );
    expect(after).toEqual({
      ...before,
    });
  });
});

describe("getFormValidityObject", () => {
  type V = {
    valid: string;
    invalid: string;
  };
  const formErrorObject: FormErrorObject<V> = {
    valid: [],
    invalid: ["エラーがあります"],
  };

  test("convert", () => {
    expect(getFormValidityObject(formErrorObject)).toEqual({
      valid: true,
      invalid: false,
    });
  });
});

describe("getFormHasVisibleErrorObject", () => {
  type V = {
    field: string;
  };

  test("valid", () => {
    const formValidityObject: FormValidityObject<V> = {
      field: true,
    };
    const formTouchedObject: FormTouchedObject<V> = {
      field: false,
    };
    expect(
      getFormHasVisibleErrorObject(formValidityObject, formTouchedObject)
    ).toEqual({
      field: false,
    });
  });

  test("invalid and untouched", () => {
    const formValidityObject: FormValidityObject<V> = {
      field: false,
    };
    const formTouchedObject: FormTouchedObject<V> = {
      field: false,
    };
    expect(
      getFormHasVisibleErrorObject(formValidityObject, formTouchedObject)
    ).toEqual({
      field: false,
    });
  });

  test("invalid and touched", () => {
    const formValidityObject: FormValidityObject<V> = {
      field: false,
    };
    const formTouchedObject: FormTouchedObject<V> = {
      field: true,
    };
    expect(
      getFormHasVisibleErrorObject(formValidityObject, formTouchedObject)
    ).toEqual({
      field: true,
    });
  });
});

describe("getFormValidity", () => {
  type V = {
    field0: string;
    field1: string;
  };

  test("all field invalid", () => {
    const formValidityObject: FormValidityObject<V> = {
      field0: false,
      field1: false,
    };
    expect(getFormValidity(formValidityObject)).toBe(false);
  });

  test("some field invalid", () => {
    const formValidityObject: FormValidityObject<V> = {
      field0: true,
      field1: false,
    };
    expect(getFormValidity(formValidityObject)).toBe(false);
  });

  test("all field valid", () => {
    const formValidityObject: FormValidityObject<V> = {
      field0: true,
      field1: true,
    };
    expect(getFormValidity(formValidityObject)).toBe(true);
  });
});

describe("getFormErrorObject", () => {
  type V = {
    field0: string;
    field1: string;
  };
  const formValueObject: V = {
    field0: "",
    field1: "",
  };

  test("initialize", () => {
    const validatorObject: ValidatorObject<V> = {
      field0: () => [],
      field1: () => ["検査済み"],
    };
    expect(getFormErrorObject(formValueObject, validatorObject)).toEqual({
      field0: [],
      field1: ["検査済み"],
    });
  });

  test("initialize with partial validatorObject", () => {
    const validatorObject: ValidatorObject<V> = {
      field1: () => ["検査済み"],
    };
    expect(getFormErrorObject(formValueObject, validatorObject)).toEqual({
      field0: [],
      field1: ["検査済み"],
    });
  });

  test("initialize without validatorObject", () => {
    expect(getFormErrorObject(formValueObject, undefined)).toEqual({
      field0: [],
      field1: [],
    });
  });
});

describe("refreshFormTouchedObject", () => {
  type V = {
    field0: string;
    field1: string;
  };
  const formValueObject: V = {
    field0: "",
    field1: "",
  };

  test("refresh with false", () => {
    expect(refreshFormTouchedObject(formValueObject, false)).toEqual({
      field0: false,
      field1: false,
    });
  });

  test("refresh with true", () => {
    expect(refreshFormTouchedObject(formValueObject, true)).toEqual({
      field0: true,
      field1: true,
    });
  });
});

describe("getFormStore", () => {
  type V = {
    text: string;
    number: number | "";
    multiItems: string[];
  };
  const formValueObject: V = {
    text: "",
    number: "",
    multiItems: [],
  };
  const formErrorObject: FormErrorObject<V> = {
    text: [],
    number: ["検査済み"],
    multiItems: ["検査済み"],
  };
  const formHasVisibleErrorObject: FormHasVisibleErrorObject<V> = {
    text: false,
    number: false,
    multiItems: true,
  };

  test("get", () => {
    expect(
      getFormStore(formValueObject, formErrorObject, formHasVisibleErrorObject)
    ).toEqual({
      text: {
        value: "",
        error: [],
        hasVisibleError: false,
      },
      number: {
        value: "",
        error: ["検査済み"],
        hasVisibleError: false,
      },
      multiItems: {
        value: [],
        error: ["検査済み"],
        hasVisibleError: true,
      },
    });
  });
});
