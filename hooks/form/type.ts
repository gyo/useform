import { ChangeEvent } from "react";

export type Errors = string[];

export type Validator<FormValueObject> = (
  formValueObject: FormValueObject
) => Errors;

export type ValidatorObject<FormValueObject> = {
  [FormValueKey in keyof FormValueObject]?: Validator<FormValueObject>;
};

export type RelationNames<FormValueObject> = (keyof FormValueObject)[];

export type RelationNamesObject<FormValueObject> = {
  [FormValueKey in keyof FormValueObject]?: RelationNames<FormValueObject>;
};

export type FormErrorObject<FormValueObject> = {
  [FormValueKey in keyof FormValueObject]: Errors;
};

export type FormValidityObject<FormValueObject> = {
  [FormValueKey in keyof FormValueObject]: boolean;
};

export type FormTouchedObject<FormValueObject> = {
  [FormValueKey in keyof FormValueObject]: boolean;
};

export type FormHasVisibleErrorObject<FormValueObject> = {
  [FormValueKey in keyof FormValueObject]: boolean;
};

export type FormStore<FormValueObject> = {
  [FormValueKey in keyof FormValueObject]: {
    value: FormValueObject[FormValueKey];
    error: Errors;
    hasVisibleError: boolean;
  };
};

export type FormValidity = boolean;

export type FormSubmitting = boolean;

export type FormState = {
  isValid: FormValidity;
  submitting: FormSubmitting;
};

export type IsChecked<FormValueObject> = (
  name: keyof FormValueObject,
  value: string
) => boolean;

export type OnChangeText<FormValueObject> = (
  name: keyof FormValueObject
) => (
  e: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>
) => void;

export type OnChangeNumber<FormValueObject> = (
  name: keyof FormValueObject
) => (e: ChangeEvent<HTMLInputElement>) => void;

export type OnChangeSelect<FormValueObject> = (
  name: keyof FormValueObject
) => (e: ChangeEvent<HTMLSelectElement>) => void;

export type OnChangeCheckbox<FormValueObject> = (
  name: keyof FormValueObject
) => (e: ChangeEvent<HTMLInputElement>) => void;

export type OnChangeRadio<FormValueObject> = (
  name: keyof FormValueObject
) => (e: ChangeEvent<HTMLInputElement>) => void;

export type OnChangeFile<FormValueObject> = (
  name: keyof FormValueObject
) => (e: ChangeEvent<HTMLInputElement>) => void;

export type OnBlur<FormValueObject> = (
  name: keyof FormValueObject
) => () => void;

export type BeginSubmit = () => void;

export type EndSubmit = () => void;

export type TouchAll = () => void;

export type OnSubmit = (args: {
  submit: () => Promise<void>;
  error?: () => void;
  beforeValidation?: () => void;
}) => (e: React.FormEvent<HTMLFormElement>) => void;

export type SetValue<FormValueObject> = (
  name: keyof FormValueObject,
  value: FormValueObject[keyof FormValueObject]
) => void;

export type SetValues<FormValueObject> = (
  values: {
    [FormValueKey in keyof FormValueObject]?: FormValueObject[keyof FormValueObject];
  }
) => void;

export type Refresh = () => void;

export type FormUtility<FormValueObject> = {
  isChecked: IsChecked<FormValueObject>;
  onChangeText: OnChangeText<FormValueObject>;
  onChangeNumber: OnChangeNumber<FormValueObject>;
  onChangeSelect: OnChangeSelect<FormValueObject>;
  onChangeCheckbox: OnChangeCheckbox<FormValueObject>;
  onChangeRadio: OnChangeRadio<FormValueObject>;
  onChangeFile: OnChangeFile<FormValueObject>;
  onBlur: OnBlur<FormValueObject>;
  beginSubmit: BeginSubmit;
  endSubmit: EndSubmit;
  touchAll: TouchAll;
  onSubmit: OnSubmit;
  setValue: SetValue<FormValueObject>;
  setValues: SetValues<FormValueObject>;
  refresh: Refresh;
};

export type UseForm<FormValueObject> = {
  store: FormStore<FormValueObject>;
  state: FormState;
  utility: FormUtility<FormValueObject>;
};
