import { act, renderHook } from "@testing-library/react-hooks";
import { ChangeEvent } from "react";
import { useOnChange } from "./useOnChange";

describe("useOnChange", () => {
  describe("validatorObject, relationNamesObject が存在する", () => {
    describe("Error が存在する", () => {
      describe("Touched が true", () => {
        test("onChangeText", () => {
          const mockSetFormValueObject = jest.fn();
          const mockSetFormErrorObject = jest.fn();
          const mockSetFormTouchedObject = jest.fn();
          const hook = renderHook(() =>
            useOnChange(
              { a: "a", b: "a", x: "" },
              mockSetFormValueObject,
              { a: [], b: [], x: ["X"] },
              mockSetFormErrorObject,
              { a: false, b: false, x: true },
              mockSetFormTouchedObject,
              {
                a: (values) => (values.a === "aa" ? ["A"] : []),
                b: (values) => (values.b !== values.a ? ["B"] : []),
                x: (values) => (values.x === "" ? ["X"] : []),
              },
              { a: ["a", "b"] }
            )
          );

          act(() => {
            const event = {
              target: { value: "aa" },
            } as
              | ChangeEvent<HTMLInputElement>
              | ChangeEvent<HTMLTextAreaElement>;

            hook.result.current.onChangeText("a")(event);
          });

          expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
            a: "aa",
            b: "a",
            x: "",
          });
          expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
            a: ["A"],
            b: ["B"],
            x: ["X"],
          });
          expect(mockSetFormTouchedObject.mock.calls.length).toBe(0);
        });

        describe("onChangeNumber", () => {
          test("value が parseInt 不可能", () => {
            const mockSetFormValueObject = jest.fn();
            const mockSetFormErrorObject = jest.fn();
            const mockSetFormTouchedObject = jest.fn();
            const hook = renderHook(() =>
              useOnChange(
                { a: 1, b: 1, x: "" },
                mockSetFormValueObject,
                { a: [], b: [], x: ["X"] },
                mockSetFormErrorObject,
                { a: false, b: false, x: true },
                mockSetFormTouchedObject,
                {
                  a: (values) => (values.a === 11 ? ["A"] : []),
                  b: (values) => (values.b !== values.a ? ["B"] : []),
                  x: (values) => (values.x === "" ? ["X"] : []),
                },
                { a: ["a", "b"] }
              )
            );

            act(() => {
              const event = {
                target: { value: "aa" },
              } as ChangeEvent<HTMLInputElement>;

              hook.result.current.onChangeNumber("a")(event);
            });

            expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
              a: "",
              b: 1,
              x: "",
            });
            expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
              a: [],
              b: ["B"],
              x: ["X"],
            });
            expect(mockSetFormTouchedObject.mock.calls.length).toBe(0);
          });

          test("value が parseInt 可能", () => {
            const mockSetFormValueObject = jest.fn();
            const mockSetFormErrorObject = jest.fn();
            const mockSetFormTouchedObject = jest.fn();
            const hook = renderHook(() =>
              useOnChange(
                { a: 1, b: 1, x: "" },
                mockSetFormValueObject,
                { a: [], b: [], x: ["X"] },
                mockSetFormErrorObject,
                { a: false, b: false, x: true },
                mockSetFormTouchedObject,
                {
                  a: (values) => (values.a === 11 ? ["A"] : []),
                  b: (values) => (values.b !== values.a ? ["B"] : []),
                  x: (values) => (values.x === "" ? ["X"] : []),
                },
                { a: ["a", "b"] }
              )
            );

            act(() => {
              const event = {
                target: { value: "11" },
              } as ChangeEvent<HTMLInputElement>;

              hook.result.current.onChangeNumber("a")(event);
            });

            expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
              a: 11,
              b: 1,
              x: "",
            });
            expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
              a: ["A"],
              b: ["B"],
              x: ["X"],
            });
            expect(mockSetFormTouchedObject.mock.calls.length).toBe(0);
          });
        });

        test("onChangeSelect", () => {
          const mockSetFormValueObject = jest.fn();
          const mockSetFormErrorObject = jest.fn();
          const mockSetFormTouchedObject = jest.fn();
          const hook = renderHook(() =>
            useOnChange(
              { a: "a", b: "a", x: "" },
              mockSetFormValueObject,
              { a: [], b: [], x: ["X"] },
              mockSetFormErrorObject,
              { a: false, b: false, x: true },
              mockSetFormTouchedObject,
              {
                a: (values) => (values.a === "aa" ? ["A"] : []),
                b: (values) => (values.b !== values.a ? ["B"] : []),
                x: (values) => (values.x === "" ? ["X"] : []),
              },
              { a: ["a", "b"] }
            )
          );

          act(() => {
            const event = {
              target: { value: "aa" },
            } as ChangeEvent<HTMLSelectElement>;

            hook.result.current.onChangeSelect("a")(event);
          });

          expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
            a: "aa",
            b: "a",
            x: "",
          });
          expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
            a: ["A"],
            b: ["B"],
            x: ["X"],
          });
          expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
            a: true,
            b: false,
            x: true,
          });
        });

        describe("onChangeCheckbox", () => {
          test("checked が false", () => {
            const mockSetFormValueObject = jest.fn();
            const mockSetFormErrorObject = jest.fn();
            const mockSetFormTouchedObject = jest.fn();
            const hook = renderHook(() =>
              useOnChange(
                { a: ["0", "1"], b: ["0", "1"], x: [] },
                mockSetFormValueObject,
                { a: [], b: [], x: ["X"] },
                mockSetFormErrorObject,
                { a: false, b: false, x: true },
                mockSetFormTouchedObject,
                {
                  a: (values) =>
                    JSON.stringify(values.a) === JSON.stringify(["0"])
                      ? ["A"]
                      : [],
                  b: (values) =>
                    JSON.stringify(values.b) !== JSON.stringify(values.a)
                      ? ["B"]
                      : [],
                  x: (values) =>
                    JSON.stringify(values.x) === JSON.stringify([])
                      ? ["X"]
                      : [],
                },
                { a: ["a", "b"] }
              )
            );

            act(() => {
              const event = {
                target: { value: "1", checked: false },
              } as ChangeEvent<HTMLInputElement>;

              hook.result.current.onChangeCheckbox("a")(event);
            });

            expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
              a: ["0"],
              b: ["0", "1"],
              x: [],
            });
            expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
              a: ["A"],
              b: ["B"],
              x: ["X"],
            });
            expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
              a: true,
              b: false,
              x: true,
            });
          });

          test("checked が true", () => {
            const mockSetFormValueObject = jest.fn();
            const mockSetFormErrorObject = jest.fn();
            const mockSetFormTouchedObject = jest.fn();
            const hook = renderHook(() =>
              useOnChange(
                { a: ["0", "1"], b: ["0", "1"], x: [] },
                mockSetFormValueObject,
                { a: [], b: [], x: ["X"] },
                mockSetFormErrorObject,
                { a: false, b: false, x: true },
                mockSetFormTouchedObject,
                {
                  a: (values) =>
                    JSON.stringify(values.a) === JSON.stringify(["0", "1", "2"])
                      ? ["A"]
                      : [],
                  b: (values) =>
                    JSON.stringify(values.b) !== JSON.stringify(values.a)
                      ? ["B"]
                      : [],
                  x: (values) =>
                    JSON.stringify(values.x) === JSON.stringify([])
                      ? ["X"]
                      : [],
                },
                { a: ["a", "b"] }
              )
            );

            act(() => {
              const event = {
                target: { value: "2", checked: true },
              } as ChangeEvent<HTMLInputElement>;

              hook.result.current.onChangeCheckbox("a")(event);
            });

            expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
              a: ["0", "1", "2"],
              b: ["0", "1"],
              x: [],
            });
            expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
              a: ["A"],
              b: ["B"],
              x: ["X"],
            });
            expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
              a: true,
              b: false,
              x: true,
            });
          });
        });

        test("onChangeRadio", () => {
          const mockSetFormValueObject = jest.fn();
          const mockSetFormErrorObject = jest.fn();
          const mockSetFormTouchedObject = jest.fn();
          const hook = renderHook(() =>
            useOnChange(
              { a: "a", b: "a", x: "" },
              mockSetFormValueObject,
              { a: [], b: [], x: ["X"] },
              mockSetFormErrorObject,
              { a: false, b: false, x: true },
              mockSetFormTouchedObject,
              {
                a: (values) => (values.a === "aa" ? ["A"] : []),
                b: (values) => (values.b !== values.a ? ["B"] : []),
                x: (values) => (values.x === "" ? ["X"] : []),
              },
              { a: ["a", "b"] }
            )
          );

          act(() => {
            const event = {
              target: { value: "aa" },
            } as ChangeEvent<HTMLInputElement>;

            hook.result.current.onChangeRadio("a")(event);
          });

          expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
            a: "aa",
            b: "a",
            x: "",
          });
          expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
            a: ["A"],
            b: ["B"],
            x: ["X"],
          });
          expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
            a: true,
            b: false,
            x: true,
          });
        });

        describe("onChangeFile", () => {
          test("files が存在しない", () => {
            const mockSetFormValueObject = jest.fn();
            const mockSetFormErrorObject = jest.fn();
            const mockSetFormTouchedObject = jest.fn();
            const hook = renderHook(() =>
              useOnChange(
                { a: ["1.png", "2.png"], b: ["1.png", "2.png"], x: "" },
                mockSetFormValueObject,
                { a: [], b: [], x: ["X"] },
                mockSetFormErrorObject,
                { a: false, b: false, x: true },
                mockSetFormTouchedObject,
                {
                  a: (values) => (values.a === null ? ["A"] : []),
                  b: (values) => (values.b !== values.a ? ["B"] : []),
                  x: (values) => (values.x === "" ? ["X"] : []),
                },
                { a: ["a", "b"] }
              )
            );

            act(() => {
              const event = {
                target: {
                  files: null,
                },
              } as ChangeEvent<HTMLInputElement>;

              hook.result.current.onChangeFile("a")(event);
            });

            expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
              a: null,
              b: ["1.png", "2.png"],
              x: "",
            });
            expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
              a: ["A"],
              b: ["B"],
              x: ["X"],
            });
            expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
              a: true,
              b: false,
              x: true,
            });
          });

          test("files を追加する", () => {
            const mockSetFormValueObject = jest.fn();
            const mockSetFormErrorObject = jest.fn();
            const mockSetFormTouchedObject = jest.fn();
            const hook = renderHook(() =>
              useOnChange(
                { a: ["1.png", "2.png"], b: ["1.png", "2.png"], x: "" },
                mockSetFormValueObject,
                { a: [], b: [], x: ["X"] },
                mockSetFormErrorObject,
                { a: false, b: false, x: true },
                mockSetFormTouchedObject,
                {
                  a: (values) =>
                    JSON.stringify(values.a) ===
                    JSON.stringify(["11.png", "22.png"])
                      ? ["A"]
                      : [],
                  b: (values) => (values.b !== values.a ? ["B"] : []),
                  x: (values) => (values.x === "" ? ["X"] : []),
                },
                { a: ["a", "b"] }
              )
            );

            act(() => {
              const event = ({
                target: {
                  files: ["11.png", "22.png"],
                },
              } as unknown) as ChangeEvent<HTMLInputElement>;

              hook.result.current.onChangeFile("a")(event);
            });

            expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
              a: ["11.png", "22.png"],
              b: ["1.png", "2.png"],
              x: "",
            });
            expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
              a: ["A"],
              b: ["B"],
              x: ["X"],
            });
            expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
              a: true,
              b: false,
              x: true,
            });
          });

          test("files を削除する", () => {
            const mockSetFormValueObject = jest.fn();
            const mockSetFormErrorObject = jest.fn();
            const mockSetFormTouchedObject = jest.fn();
            const hook = renderHook(() =>
              useOnChange(
                { a: ["1.png", "2.png"], b: ["1.png", "2.png"], x: "" },
                mockSetFormValueObject,
                { a: [], b: [], x: ["X"] },
                mockSetFormErrorObject,
                { a: false, b: false, x: true },
                mockSetFormTouchedObject,
                {
                  a: (values) => (values.a === null ? ["A"] : []),
                  b: (values) => (values.b !== values.a ? ["B"] : []),
                  x: (values) => (values.x === "" ? ["X"] : []),
                },
                { a: ["a", "b"] }
              )
            );

            act(() => {
              const event = {
                target: {
                  files: null,
                },
              } as ChangeEvent<HTMLInputElement>;

              hook.result.current.onChangeFile("a")(event);
            });

            expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
              a: null,
              b: ["1.png", "2.png"],
              x: "",
            });
            expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
              a: ["A"],
              b: ["B"],
              x: ["X"],
            });
            expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
              a: true,
              b: false,
              x: true,
            });
          });
        });
      });
    });
  });
});
