import { act, renderHook } from "@testing-library/react-hooks";
import { useOnBlur } from "./useOnBlur";

describe("useOnBlur", () => {
  describe("form の要素が一つ", () => {
    test("Touched が false", () => {
      const mockSetFormTouchedObject = jest.fn();
      const hook = renderHook(() =>
        useOnBlur({ name: false }, mockSetFormTouchedObject)
      );

      act(() => {
        hook.result.current.onBlur("name")();
      });

      expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({ name: true });
    });

    test("Touched が true", () => {
      const mockSetFormTouchedObject = jest.fn();
      const hook = renderHook(() =>
        useOnBlur({ name: true }, mockSetFormTouchedObject)
      );

      act(() => {
        hook.result.current.onBlur("name")();
      });

      expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({ name: true });
    });
  });

  describe("form の要素が複数", () => {
    test("Touched が false", () => {
      const mockSetFormTouchedObject = jest.fn();
      const hook = renderHook(() =>
        useOnBlur(
          { name: false, age: true, items: false },
          mockSetFormTouchedObject
        )
      );

      act(() => {
        hook.result.current.onBlur("name")();
      });

      expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
        name: true,
        age: true,
        items: false,
      });
    });

    test("Touched が true", () => {
      const mockSetFormTouchedObject = jest.fn();
      const hook = renderHook(() =>
        useOnBlur(
          { name: true, age: true, items: false },
          mockSetFormTouchedObject
        )
      );

      act(() => {
        hook.result.current.onBlur("name")();
      });

      expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
        name: true,
        age: true,
        items: false,
      });
    });
  });
});
