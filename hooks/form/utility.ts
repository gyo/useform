import {
  FormErrorObject,
  FormHasVisibleErrorObject,
  FormStore,
  FormTouchedObject,
  FormValidity,
  FormValidityObject,
  RelationNames,
  RelationNamesObject,
  ValidatorObject,
} from "./type";

export const getNextFormErrorObject = <FormValueObject>(
  currentFormErrorObject: FormErrorObject<FormValueObject>,
  names: (keyof FormValueObject)[],
  formValueObject: FormValueObject,
  validatorObject?: ValidatorObject<FormValueObject>,
  relationNamesObject?: RelationNamesObject<FormValueObject>
): FormErrorObject<FormValueObject> => {
  const relationNames = names.flatMap((name) => {
    if (relationNamesObject == null || relationNamesObject[name] == null) {
      return name;
    }
    return relationNamesObject[name] as RelationNames<FormValueObject>;
  });

  return relationNames.reduce<FormErrorObject<FormValueObject>>(
    (newFormErrorObject, relationName) => {
      if (validatorObject == null) {
        return newFormErrorObject;
      }
      const validator = validatorObject[relationName];
      if (validator != null) {
        newFormErrorObject[relationName] = validator(formValueObject);
      }
      return newFormErrorObject;
    },
    { ...currentFormErrorObject }
  );
};

export const getFormValidityObject = <FormValueObject>(
  formErrorObject: FormErrorObject<FormValueObject>
): FormValidityObject<FormValueObject> => {
  const names = Object.keys(
    formErrorObject
  ) as (keyof FormErrorObject<FormValueObject>)[];
  return names.reduce<FormValidityObject<FormValueObject>>(
    (formValidityObject, name) => {
      formValidityObject[name] = formErrorObject[name].length === 0;
      return formValidityObject;
    },
    {} as FormValidityObject<FormValueObject>
  );
};

export const getFormHasVisibleErrorObject = <FormValueObject>(
  formValidityObject: FormValidityObject<FormValueObject>,
  formTouchedObject: FormTouchedObject<FormValueObject>
): FormHasVisibleErrorObject<FormValueObject> => {
  const names = Object.keys(
    formValidityObject
  ) as (keyof FormValidityObject<FormValueObject>)[];
  return names.reduce<FormHasVisibleErrorObject<FormValueObject>>(
    (formHasVisibleErrorObject, name) => {
      formHasVisibleErrorObject[name] =
        !formValidityObject[name] && formTouchedObject[name];
      return formHasVisibleErrorObject;
    },
    {} as FormHasVisibleErrorObject<FormValueObject>
  );
};

export const getFormValidity = <FormValueObject>(
  formValidityObject: FormValidityObject<FormValueObject>
): FormValidity => {
  const names = Object.keys(
    formValidityObject
  ) as (keyof FormValidityObject<FormValueObject>)[];
  return names.every((name) => {
    return formValidityObject[name];
  });
};

export const getFormErrorObject = <FormValueObject>(
  formValues: FormValueObject,
  validatorObject?: ValidatorObject<FormValueObject>
): FormErrorObject<FormValueObject> => {
  const names = Object.keys(formValues) as (keyof FormValueObject)[];
  return names.reduce<FormErrorObject<FormValueObject>>(
    (formErrorObject, name) => {
      if (validatorObject == null) {
        formErrorObject[name] = [] as string[];
      } else {
        const validatorName = validatorObject[name];
        if (validatorName != null) {
          formErrorObject[name] = validatorName(formValues);
        } else {
          formErrorObject[name] = [];
        }
      }
      return formErrorObject;
    },
    {} as FormErrorObject<FormValueObject>
  );
};

export const refreshFormTouchedObject = <FormValueObject>(
  formValueObject: FormValueObject,
  touchedValue: boolean
): FormTouchedObject<FormValueObject> => {
  const names = Object.keys(formValueObject) as (keyof FormValueObject)[];
  return names.reduce<FormTouchedObject<FormValueObject>>(
    (formTouchedObject, name) => {
      formTouchedObject[name] = touchedValue;
      return formTouchedObject;
    },
    {} as FormTouchedObject<FormValueObject>
  );
};

export const getFormStore = <FormValueObject>(
  formValueObject: FormValueObject,
  formErrorObject: FormErrorObject<FormValueObject>,
  formHasVisibleErrorObject: FormHasVisibleErrorObject<FormValueObject>
): FormStore<FormValueObject> => {
  const names = Object.keys(formValueObject) as (keyof FormValueObject)[];
  return names.reduce<FormStore<FormValueObject>>((formStore, name) => {
    formStore[name] = {
      value: formValueObject[name],
      error: formErrorObject[name],
      hasVisibleError: formHasVisibleErrorObject[name],
    };
    return formStore;
  }, {} as FormStore<FormValueObject>);
};
