import { Dispatch, SetStateAction, useCallback } from "react";
import {
  FormErrorObject,
  FormSubmitting,
  FormTouchedObject,
  Refresh,
  RelationNamesObject,
  SetValue,
  SetValues,
  ValidatorObject,
} from "./type";
import {
  getFormErrorObject,
  getNextFormErrorObject,
  refreshFormTouchedObject,
} from "./utility";

export const useSetValue = <FormValueObject>(
  formValueObject: FormValueObject,
  setFormValueObject: Dispatch<SetStateAction<FormValueObject>>,
  formErrorObject: FormErrorObject<FormValueObject>,
  setFormErrorObject: Dispatch<
    SetStateAction<FormErrorObject<FormValueObject>>
  >,
  setFormTouchedObject: Dispatch<
    SetStateAction<FormTouchedObject<FormValueObject>>
  >,
  setFormSubmitting: Dispatch<SetStateAction<FormSubmitting>>,
  initialFormValueObject: FormValueObject,
  validatorObject?: ValidatorObject<FormValueObject>,
  relationNamesObject?: RelationNamesObject<FormValueObject>
) => {
  const setValue: SetValue<FormValueObject> = useCallback(
    (name, value) => {
      const nextFormValueObject = { ...formValueObject, [name]: value };
      const nextFormErrorObject = getNextFormErrorObject(
        formErrorObject,
        [name],
        nextFormValueObject,
        validatorObject,
        relationNamesObject
      );

      setFormValueObject(nextFormValueObject);
      setFormErrorObject(nextFormErrorObject);
    },
    [
      formErrorObject,
      formValueObject,
      relationNamesObject,
      setFormErrorObject,
      setFormValueObject,
      validatorObject,
    ]
  );

  const setValues: SetValues<FormValueObject> = useCallback(
    (values) => {
      const nextFormValueObject = { ...formValueObject, ...values };
      const nextFormErrorObject = getNextFormErrorObject(
        formErrorObject,
        Object.keys(values) as (keyof FormValueObject)[],
        nextFormValueObject,
        validatorObject,
        relationNamesObject
      );

      setFormValueObject(nextFormValueObject);
      setFormErrorObject(nextFormErrorObject);
    },
    [
      formErrorObject,
      formValueObject,
      relationNamesObject,
      setFormErrorObject,
      setFormValueObject,
      validatorObject,
    ]
  );

  const refresh: Refresh = useCallback(() => {
    const nextFormValueObject = initialFormValueObject;
    const nextFormErrorObject = getFormErrorObject(
      nextFormValueObject,
      validatorObject
    );
    const nextFormTouchedObject = refreshFormTouchedObject(
      nextFormValueObject,
      false
    );

    setFormValueObject(nextFormValueObject);
    setFormErrorObject(nextFormErrorObject);
    setFormTouchedObject(nextFormTouchedObject);
    setFormSubmitting(false);
  }, [
    initialFormValueObject,
    setFormErrorObject,
    setFormSubmitting,
    setFormTouchedObject,
    setFormValueObject,
    validatorObject,
  ]);

  return {
    setValue,
    setValues,
    refresh,
  };
};
