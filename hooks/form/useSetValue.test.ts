import { act, renderHook } from "@testing-library/react-hooks";
import { useSetValue } from "./useSetValue";

describe("useSetValue", () => {
  describe("setValue", () => {
    test("validatorObject, relationNamesObject が存在しない", () => {
      const mockSetFormValueObject = jest.fn();
      const mockSetFormErrorObject = jest.fn();
      const mockSetFormTouchedObject = jest.fn();
      const mockSetFormSubmitting = jest.fn();
      const hook = renderHook(() =>
        useSetValue(
          { a: "aa", b: "aa", x: "x" },
          mockSetFormValueObject,
          { a: [], b: [], x: [] },
          mockSetFormErrorObject,
          mockSetFormTouchedObject,
          mockSetFormSubmitting,
          { a: "a", b: "b", x: "x" }
        )
      );

      act(() => {
        hook.result.current.setValue("a", "aaa");
      });

      expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
        a: "aaa",
        b: "aa",
        x: "x",
      });
      expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
        a: [],
        b: [],
        x: [],
      });
    });

    test("validatorObject, relationNamesObject が存在する", () => {
      const mockSetFormValueObject = jest.fn();
      const mockSetFormErrorObject = jest.fn();
      const mockSetFormTouchedObject = jest.fn();
      const mockSetFormSubmitting = jest.fn();
      const hook = renderHook(() =>
        useSetValue(
          { a: "aa", b: "aa", x: "x" },
          mockSetFormValueObject,
          { a: [], b: [], x: [] },
          mockSetFormErrorObject,
          mockSetFormTouchedObject,
          mockSetFormSubmitting,
          { a: "a", b: "b", x: "x" },
          {
            a: (values) => (values.a === "aaa" ? ["A"] : []),
            b: (values) => (values.b !== values.a ? ["B"] : []),
          },
          { a: ["a", "b"] }
        )
      );

      act(() => {
        hook.result.current.setValue("a", "aaa");
      });

      expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
        a: "aaa",
        b: "aa",
        x: "x",
      });
      expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
        a: ["A"],
        b: ["B"],
        x: [],
      });
    });
  });

  describe("setValues", () => {
    test("validatorObject, relationNamesObject が存在しない", () => {
      const mockSetFormValueObject = jest.fn();
      const mockSetFormErrorObject = jest.fn();
      const mockSetFormTouchedObject = jest.fn();
      const mockSetFormSubmitting = jest.fn();
      const hook = renderHook(() =>
        useSetValue(
          { a: "aa", b: "aa", x: "x" },
          mockSetFormValueObject,
          { a: [], b: [], x: [] },
          mockSetFormErrorObject,
          mockSetFormTouchedObject,
          mockSetFormSubmitting,
          { a: "a", b: "b", x: "x" }
        )
      );

      act(() => {
        hook.result.current.setValues({
          a: "aaa",
          b: "bbb",
        });
      });

      expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
        a: "aaa",
        b: "bbb",
        x: "x",
      });
      expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
        a: [],
        b: [],
        x: [],
      });
    });

    test("validatorObject, relationNamesObject が存在する", () => {
      const mockSetFormValueObject = jest.fn();
      const mockSetFormErrorObject = jest.fn();
      const mockSetFormTouchedObject = jest.fn();
      const mockSetFormSubmitting = jest.fn();
      const hook = renderHook(() =>
        useSetValue(
          { a: "aa", b: "aa", x: "x" },
          mockSetFormValueObject,
          { a: [], b: [], x: [] },
          mockSetFormErrorObject,
          mockSetFormTouchedObject,
          mockSetFormSubmitting,
          { a: "a", b: "b", x: "x" },
          {
            a: (values) => (values.a === "aaa" ? ["A"] : []),
            b: (values) => (values.b !== values.a ? ["B"] : []),
          },
          { a: ["a", "b"] }
        )
      );

      act(() => {
        hook.result.current.setValues({
          a: "aaa",
          b: "bbb",
        });
      });

      expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
        a: "aaa",
        b: "bbb",
        x: "x",
      });
      expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
        a: ["A"],
        b: ["B"],
        x: [],
      });
    });
  });

  describe("refresh", () => {
    test("validatorObject, relationNamesObject が存在しない", () => {
      const mockSetFormValueObject = jest.fn();
      const mockSetFormErrorObject = jest.fn();
      const mockSetFormTouchedObject = jest.fn();
      const mockSetFormSubmitting = jest.fn();
      const hook = renderHook(() =>
        useSetValue(
          { a: "aa", b: "aa", x: "x" },
          mockSetFormValueObject,
          { a: [], b: [], x: [] },
          mockSetFormErrorObject,
          mockSetFormTouchedObject,
          mockSetFormSubmitting,
          { a: "a", b: "b", x: "x" }
        )
      );

      act(() => {
        hook.result.current.refresh();
      });

      expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
        a: "a",
        b: "b",
        x: "x",
      });
      expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
        a: [],
        b: [],
        x: [],
      });
      expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
        a: false,
        b: false,
        x: false,
      });
      expect(mockSetFormSubmitting.mock.calls[0][0]).toEqual(false);
    });

    test("validatorObject, relationNamesObject が存在する", () => {
      const mockSetFormValueObject = jest.fn();
      const mockSetFormErrorObject = jest.fn();
      const mockSetFormTouchedObject = jest.fn();
      const mockSetFormSubmitting = jest.fn();
      const hook = renderHook(() =>
        useSetValue(
          { a: "aa", b: "aa", x: "x" },
          mockSetFormValueObject,
          { a: [], b: [], x: [] },
          mockSetFormErrorObject,
          mockSetFormTouchedObject,
          mockSetFormSubmitting,
          { a: "a", b: "b", x: "x" },
          {
            a: (values) => (values.a === "aaa" ? ["A"] : []),
            b: (values) => (values.b !== values.a ? ["B"] : []),
          },
          { a: ["a", "b"] }
        )
      );

      act(() => {
        hook.result.current.refresh();
      });

      expect(mockSetFormValueObject.mock.calls[0][0]).toEqual({
        a: "a",
        b: "b",
        x: "x",
      });
      expect(mockSetFormErrorObject.mock.calls[0][0]).toEqual({
        a: [],
        b: ["B"],
        x: [],
      });
      expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
        a: false,
        b: false,
        x: false,
      });
      expect(mockSetFormSubmitting.mock.calls[0][0]).toEqual(false);
    });
  });
});
