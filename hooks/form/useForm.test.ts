import { renderHook } from "@testing-library/react-hooks";
import { useForm } from "./useForm";

describe("useForm", () => {
  test("validatorObject, relationNamesObject が存在しない", () => {
    const hook = renderHook(() => useForm({ a: "a", b: "b" }));

    expect(hook.result.current.store).toEqual({
      a: { value: "a", error: [], hasVisibleError: false },
      b: { value: "b", error: [], hasVisibleError: false },
    });
    expect(hook.result.current.state).toEqual({
      isValid: true,
      submitting: false,
    });

    // hook.result.current.utility のプロパティについては、値の正しさまでは確認していない
    expect(hook.result.current.utility).toHaveProperty("isChecked");
    expect(hook.result.current.utility).toHaveProperty("onChangeText");
    expect(hook.result.current.utility).toHaveProperty("onChangeNumber");
    expect(hook.result.current.utility).toHaveProperty("onChangeSelect");
    expect(hook.result.current.utility).toHaveProperty("onChangeCheckbox");
    expect(hook.result.current.utility).toHaveProperty("onChangeRadio");
    expect(hook.result.current.utility).toHaveProperty("onChangeFile");
    expect(hook.result.current.utility).toHaveProperty("onBlur");
    expect(hook.result.current.utility).toHaveProperty("beginSubmit");
    expect(hook.result.current.utility).toHaveProperty("endSubmit");
    expect(hook.result.current.utility).toHaveProperty("touchAll");
    expect(hook.result.current.utility).toHaveProperty("onSubmit");
    expect(hook.result.current.utility).toHaveProperty("setValue");
    expect(hook.result.current.utility).toHaveProperty("setValues");
    expect(hook.result.current.utility).toHaveProperty("refresh");
  });

  test("validatorObject, relationNamesObject が存在する", () => {
    const hook = renderHook(() =>
      useForm(
        { a: "a", b: "b" },
        {
          a: (values) => (values.a === "" ? ["A"] : []),
          b: (values) => (values.b === "b" ? ["B"] : []),
        },
        { a: ["a", "b"] }
      )
    );

    expect(hook.result.current.store).toEqual({
      a: { value: "a", error: [], hasVisibleError: false },
      b: { value: "b", error: ["B"], hasVisibleError: false },
    });
    expect(hook.result.current.state).toEqual({
      isValid: false,
      submitting: false,
    });

    // hook.result.current.utility のプロパティについては、値の正しさまでは確認していない
    expect(hook.result.current.utility).toHaveProperty("isChecked");
    expect(hook.result.current.utility).toHaveProperty("onChangeText");
    expect(hook.result.current.utility).toHaveProperty("onChangeNumber");
    expect(hook.result.current.utility).toHaveProperty("onChangeSelect");
    expect(hook.result.current.utility).toHaveProperty("onChangeCheckbox");
    expect(hook.result.current.utility).toHaveProperty("onChangeRadio");
    expect(hook.result.current.utility).toHaveProperty("onChangeFile");
    expect(hook.result.current.utility).toHaveProperty("onBlur");
    expect(hook.result.current.utility).toHaveProperty("beginSubmit");
    expect(hook.result.current.utility).toHaveProperty("endSubmit");
    expect(hook.result.current.utility).toHaveProperty("touchAll");
    expect(hook.result.current.utility).toHaveProperty("onSubmit");
    expect(hook.result.current.utility).toHaveProperty("setValue");
    expect(hook.result.current.utility).toHaveProperty("setValues");
    expect(hook.result.current.utility).toHaveProperty("refresh");
  });
});
