import { useCallback } from "react";
import { IsChecked } from "./type";

export const useIsChecked = <FormValueObject>(
  formValueObject: FormValueObject
) => {
  const isChecked: IsChecked<FormValueObject> = useCallback(
    (name, value) => {
      const formValueValue = formValueObject[name];
      if (typeof formValueValue === "string") {
        return value === formValueValue;
      }
      if (Array.isArray(formValueValue)) {
        return formValueValue.includes(value);
      }
      return false;
    },
    [formValueObject]
  );

  return {
    isChecked,
  };
};
