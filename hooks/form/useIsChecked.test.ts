import { renderHook } from "@testing-library/react-hooks";
import { useIsChecked } from "./useIsChecked";

describe("useIsChecked", () => {
  describe("文字列型の単一選択（Radio）の場合", () => {
    test("checked である", () => {
      const hook = renderHook(() => useIsChecked({ name: "a" }));

      expect(hook.result.current.isChecked("name", "a")).toBe(true);
    });

    test("checked でない", () => {
      const hook = renderHook(() => useIsChecked({ name: "a" }));

      expect(hook.result.current.isChecked("name", "z")).toBe(false);
    });
  });

  describe("文字列型の複数選択（Checkbox）の場合", () => {
    test("checked である", () => {
      const hook = renderHook(() => useIsChecked({ name: ["a", "b"] }));

      expect(hook.result.current.isChecked("name", "a")).toBe(true);
    });

    test("checked でない", () => {
      const hook = renderHook(() => useIsChecked({ name: ["a", "b"] }));

      expect(hook.result.current.isChecked("name", "z")).toBe(false);
    });
  });

  describe("数値型の場合", () => {
    test("checked でない", () => {
      const hook = renderHook(() => useIsChecked({ name: 1 }));

      expect(hook.result.current.isChecked("name", "1")).toBe(false);
    });
  });
});
