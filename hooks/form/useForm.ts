import { useState } from "react";
import { RelationNamesObject, UseForm, ValidatorObject } from "./type";
import { useIsChecked } from "./useIsChecked";
import { useMemoValues } from "./useMemoValues";
import { useOnBlur } from "./useOnBlur";
import { useOnChange } from "./useOnChange";
import { useOnSubmit } from "./useOnSubmit";
import { useSetValue } from "./useSetValue";
import { getFormErrorObject, refreshFormTouchedObject } from "./utility";

export const useForm = <FormValueObject>(
  initialFormValueObject: FormValueObject,
  validatorObject?: ValidatorObject<FormValueObject>,
  relationNamesObject?: RelationNamesObject<FormValueObject>
): UseForm<FormValueObject> => {
  const [formValueObject, setFormValueObject] = useState<FormValueObject>(
    initialFormValueObject
  );
  const [formErrorObject, setFormErrorObject] = useState(
    getFormErrorObject(formValueObject, validatorObject)
  );
  const [formTouchedObject, setFormTouchedObject] = useState(
    refreshFormTouchedObject(formValueObject, false)
  );
  const [formSubmitting, setFormSubmitting] = useState(false);

  const { formStore, formValidity } = useMemoValues(
    formValueObject,
    formErrorObject,
    formTouchedObject
  );

  const { isChecked } = useIsChecked(formValueObject);

  const {
    onChangeText,
    onChangeNumber,
    onChangeSelect,
    onChangeCheckbox,
    onChangeRadio,
    onChangeFile,
  } = useOnChange(
    formValueObject,
    setFormValueObject,
    formErrorObject,
    setFormErrorObject,
    formTouchedObject,
    setFormTouchedObject,
    validatorObject,
    relationNamesObject
  );

  const { onBlur } = useOnBlur(formTouchedObject, setFormTouchedObject);

  const { beginSubmit, endSubmit, touchAll, onSubmit } = useOnSubmit(
    formValueObject,
    setFormTouchedObject,
    formSubmitting,
    setFormSubmitting,
    formValidity
  );

  const { setValue, setValues, refresh } = useSetValue(
    formValueObject,
    setFormValueObject,
    formErrorObject,
    setFormErrorObject,
    setFormTouchedObject,
    setFormSubmitting,
    initialFormValueObject,
    validatorObject,
    relationNamesObject
  );

  return {
    store: formStore,
    state: {
      isValid: formValidity,
      submitting: formSubmitting,
    },
    utility: {
      isChecked,
      onChangeText,
      onChangeNumber,
      onChangeSelect,
      onChangeCheckbox,
      onChangeRadio,
      onChangeFile,
      onBlur,
      beginSubmit,
      endSubmit,
      touchAll,
      onSubmit,
      setValue,
      setValues,
      refresh,
    },
  };
};
