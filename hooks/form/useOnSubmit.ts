import { Dispatch, SetStateAction, useCallback } from "react";
import {
  BeginSubmit,
  EndSubmit,
  FormSubmitting,
  FormTouchedObject,
  FormValidity,
  OnSubmit,
  TouchAll,
} from "./type";
import { refreshFormTouchedObject } from "./utility";

export const useOnSubmit = <FormValueObject>(
  formValueObject: FormValueObject,
  setFormTouchedObject: Dispatch<
    SetStateAction<FormTouchedObject<FormValueObject>>
  >,
  formSubmitting: FormSubmitting,
  setFormSubmitting: Dispatch<SetStateAction<FormSubmitting>>,
  formValidity: FormValidity
) => {
  const beginSubmit: BeginSubmit = useCallback(() => {
    setFormSubmitting(true);
  }, [setFormSubmitting]);

  const endSubmit: EndSubmit = useCallback(() => {
    setFormSubmitting(false);
  }, [setFormSubmitting]);

  const touchAll: TouchAll = useCallback(() => {
    const nextFormTouchedObject = refreshFormTouchedObject(
      formValueObject,
      true
    );

    setFormTouchedObject(nextFormTouchedObject);
  }, [formValueObject, setFormTouchedObject]);

  const onSubmit: OnSubmit = ({ submit, error, beforeValidation }) => (e) => {
    e.preventDefault();

    beforeValidation?.();

    if (formSubmitting) {
      return;
    }

    if (formValidity) {
      (async () => {
        beginSubmit();
        await submit();
        endSubmit();
      })();
    } else {
      (async () => {
        touchAll();
        error?.();
      })();
    }
  };

  return {
    beginSubmit,
    endSubmit,
    touchAll,
    onSubmit,
  };
};
