import { Dispatch, SetStateAction, useCallback } from "react";
import {
  FormErrorObject,
  FormTouchedObject,
  OnChangeCheckbox,
  OnChangeFile,
  OnChangeNumber,
  OnChangeRadio,
  OnChangeSelect,
  OnChangeText,
  RelationNamesObject,
  ValidatorObject,
} from "./type";
import { getNextFormErrorObject } from "./utility";

export const useOnChange = <FormValueObject>(
  formValueObject: FormValueObject,
  setFormValueObject: Dispatch<SetStateAction<FormValueObject>>,
  formErrorObject: FormErrorObject<FormValueObject>,
  setFormErrorObject: Dispatch<
    SetStateAction<FormErrorObject<FormValueObject>>
  >,
  formTouchedObject: FormTouchedObject<FormValueObject>,
  setFormTouchedObject: Dispatch<
    SetStateAction<FormTouchedObject<FormValueObject>>
  >,
  validatorObject?: ValidatorObject<FormValueObject>,
  relationNamesObject?: RelationNamesObject<FormValueObject>
) => {
  const onChangeText: OnChangeText<FormValueObject> = useCallback(
    (name) => (e) => {
      const nextValue = e.target.value;
      const nextFormValueObject = { ...formValueObject, [name]: nextValue };
      const nextFormErrorObject = getNextFormErrorObject(
        formErrorObject,
        [name],
        nextFormValueObject,
        validatorObject,
        relationNamesObject
      );

      setFormValueObject(nextFormValueObject);
      setFormErrorObject(nextFormErrorObject);
    },
    [
      formErrorObject,
      formValueObject,
      relationNamesObject,
      setFormErrorObject,
      setFormValueObject,
      validatorObject,
    ]
  );

  const onChangeNumber: OnChangeNumber<FormValueObject> = useCallback(
    (name) => (e) => {
      const targetValue = parseInt(e.target.value, 10);
      const nextValue = isNaN(targetValue) ? "" : targetValue;
      const nextFormValueObject = { ...formValueObject, [name]: nextValue };
      const nextFormErrorObject = getNextFormErrorObject(
        formErrorObject,
        [name],
        nextFormValueObject,
        validatorObject,
        relationNamesObject
      );

      setFormValueObject(nextFormValueObject);
      setFormErrorObject(nextFormErrorObject);
    },
    [
      formErrorObject,
      formValueObject,
      relationNamesObject,
      setFormErrorObject,
      setFormValueObject,
      validatorObject,
    ]
  );

  const onChangeSelect: OnChangeSelect<FormValueObject> = useCallback(
    (name) => (e) => {
      const nextValue = e.target.value;
      const nextFormValueObject = { ...formValueObject, [name]: nextValue };
      const nextFormErrorObject = getNextFormErrorObject(
        formErrorObject,
        [name],
        nextFormValueObject,
        validatorObject,
        relationNamesObject
      );
      const nextFormTouchedObject = { ...formTouchedObject, [name]: true };

      setFormTouchedObject(nextFormTouchedObject);
      setFormValueObject(nextFormValueObject);
      setFormErrorObject(nextFormErrorObject);
    },
    [
      formErrorObject,
      formTouchedObject,
      formValueObject,
      relationNamesObject,
      setFormErrorObject,
      setFormTouchedObject,
      setFormValueObject,
      validatorObject,
    ]
  );

  const onChangeCheckbox: OnChangeCheckbox<FormValueObject> = useCallback(
    (name) => (e) => {
      const targetValue = e.target.value;
      const targetChecked = e.target.checked;
      const currentValue = (formValueObject[name] as unknown) as string[];
      const nextValue = targetChecked
        ? [...currentValue, targetValue]
        : currentValue.filter((item) => item !== targetValue);
      const nextFormValueObject = { ...formValueObject, [name]: nextValue };
      const nextFormErrorObject = getNextFormErrorObject(
        formErrorObject,
        [name],
        nextFormValueObject,
        validatorObject,
        relationNamesObject
      );
      const nextFormTouchedObject = { ...formTouchedObject, [name]: true };

      setFormTouchedObject(nextFormTouchedObject);
      setFormValueObject(nextFormValueObject);
      setFormErrorObject(nextFormErrorObject);
    },
    [
      formErrorObject,
      formTouchedObject,
      formValueObject,
      relationNamesObject,
      setFormErrorObject,
      setFormTouchedObject,
      setFormValueObject,
      validatorObject,
    ]
  );

  const onChangeRadio: OnChangeRadio<FormValueObject> = useCallback(
    (name) => (e) => {
      const nextValue = e.target.value;
      const nextFormValueObject = { ...formValueObject, [name]: nextValue };
      const nextFormErrorObject = getNextFormErrorObject(
        formErrorObject,
        [name],
        nextFormValueObject,
        validatorObject,
        relationNamesObject
      );
      const nextFormTouchedObject = { ...formTouchedObject, [name]: true };

      setFormTouchedObject(nextFormTouchedObject);
      setFormValueObject(nextFormValueObject);
      setFormErrorObject(nextFormErrorObject);
    },
    [
      formErrorObject,
      formTouchedObject,
      formValueObject,
      relationNamesObject,
      setFormErrorObject,
      setFormTouchedObject,
      setFormValueObject,
      validatorObject,
    ]
  );

  const onChangeFile: OnChangeFile<FormValueObject> = useCallback(
    (name) => (e) => {
      const rawFiles = e.target.files;
      const nextValue = rawFiles == null ? null : Array.from(rawFiles);
      const nextFormValueObject = { ...formValueObject, [name]: nextValue };
      const nextFormErrorObject = getNextFormErrorObject(
        formErrorObject,
        [name],
        nextFormValueObject,
        validatorObject,
        relationNamesObject
      );
      const nextFormTouchedObject = { ...formTouchedObject, [name]: true };

      setFormTouchedObject(nextFormTouchedObject);
      setFormValueObject(nextFormValueObject);
      setFormErrorObject(nextFormErrorObject);
    },
    [
      formErrorObject,
      formTouchedObject,
      formValueObject,
      relationNamesObject,
      setFormErrorObject,
      setFormTouchedObject,
      setFormValueObject,
      validatorObject,
    ]
  );

  return {
    onChangeText,
    onChangeNumber,
    onChangeSelect,
    onChangeCheckbox,
    onChangeRadio,
    onChangeFile,
  };
};
