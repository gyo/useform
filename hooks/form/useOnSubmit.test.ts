import { act, renderHook } from "@testing-library/react-hooks";
import { FormEvent } from "react";
import { wait } from "../../utilities/wait";
import { useOnSubmit } from "./useOnSubmit";

describe("useOnSubmit", () => {
  test("beginSubmit", () => {
    const mockSetFormTouchedObject = jest.fn();
    const mockSetFormSubmitting = jest.fn();
    const hook = renderHook(() =>
      useOnSubmit(
        {},
        mockSetFormTouchedObject,
        false,
        mockSetFormSubmitting,
        false
      )
    );

    act(() => {
      hook.result.current.beginSubmit();
    });

    expect(mockSetFormSubmitting.mock.calls[0][0]).toBe(true);
  });

  test("endSubmit", () => {
    const mockSetFormTouchedObject = jest.fn();
    const mockSetFormSubmitting = jest.fn();
    const hook = renderHook(() =>
      useOnSubmit(
        {},
        mockSetFormTouchedObject,
        true,
        mockSetFormSubmitting,
        false
      )
    );

    act(() => {
      hook.result.current.endSubmit();
    });

    expect(mockSetFormSubmitting.mock.calls[0][0]).toBe(false);
  });

  test("touchAll", () => {
    const mockSetFormTouchedObject = jest.fn();
    const mockSetFormSubmitting = jest.fn();
    const hook = renderHook(() =>
      useOnSubmit(
        { a: "a", b: "b" },
        mockSetFormTouchedObject,
        false,
        mockSetFormSubmitting,
        false
      )
    );

    act(() => {
      hook.result.current.touchAll();
    });

    expect(mockSetFormTouchedObject.mock.calls[0][0]).toEqual({
      a: true,
      b: true,
    });
  });

  describe("onSubmit", () => {
    test("beforeValidation が空である", () => {
      const mockSetFormTouchedObject = jest.fn();
      const mockSetFormSubmitting = jest.fn();
      const mockSubmit = jest.fn();
      const mockError = jest.fn();
      const hook = renderHook(() =>
        useOnSubmit(
          { a: "a", b: "b" },
          mockSetFormTouchedObject,
          false,
          mockSetFormSubmitting,
          true
        )
      );

      act(() => {
        const event = {
          preventDefault: () => {
            /*  */
          },
        } as FormEvent<HTMLFormElement>;

        hook.result.current.onSubmit({
          submit: mockSubmit,
          error: mockError,
        })(event);
      });

      expect(mockSetFormTouchedObject.mock.calls.length).toBe(0);
      expect(mockSetFormSubmitting.mock.calls.length).toBe(1);
      expect(mockSubmit.mock.calls.length).toBe(1);
      expect(mockError.mock.calls.length).toBe(0);
    });

    describe("formValidity が false である", () => {
      test("コールバック error が存在しない", () => {
        const mockSetFormTouchedObject = jest.fn();
        const mockSetFormSubmitting = jest.fn();
        const mockSubmit = jest.fn();
        const mockBeforeValidation = jest.fn();
        const hook = renderHook(() =>
          useOnSubmit(
            { a: "a", b: "b" },
            mockSetFormTouchedObject,
            false,
            mockSetFormSubmitting,
            false
          )
        );

        act(() => {
          const event = {
            preventDefault: () => {
              /*  */
            },
          } as FormEvent<HTMLFormElement>;

          hook.result.current.onSubmit({
            submit: mockSubmit,
            beforeValidation: mockBeforeValidation,
          })(event);
        });

        expect(mockSetFormTouchedObject.mock.calls.length).toBe(1);
        expect(mockSetFormSubmitting.mock.calls.length).toBe(0);
        expect(mockSubmit.mock.calls.length).toBe(0);
        expect(mockBeforeValidation.mock.calls.length).toBe(1);
      });

      test("コールバック error が存在する", () => {
        const mockSetFormTouchedObject = jest.fn();
        const mockSetFormSubmitting = jest.fn();
        const mockSubmit = jest.fn();
        const mockError = jest.fn();
        const mockBeforeValidation = jest.fn();
        const hook = renderHook(() =>
          useOnSubmit(
            { a: "a", b: "b" },
            mockSetFormTouchedObject,
            false,
            mockSetFormSubmitting,
            false
          )
        );

        act(() => {
          const event = {
            preventDefault: () => {
              /*  */
            },
          } as FormEvent<HTMLFormElement>;

          hook.result.current.onSubmit({
            submit: mockSubmit,
            error: mockError,
            beforeValidation: mockBeforeValidation,
          })(event);
        });

        expect(mockSetFormTouchedObject.mock.calls.length).toBe(1);
        expect(mockSetFormSubmitting.mock.calls.length).toBe(0);
        expect(mockSubmit.mock.calls.length).toBe(0);
        expect(mockError.mock.calls.length).toBe(1);
        expect(mockBeforeValidation.mock.calls.length).toBe(1);
      });
    });

    describe("formValidity が true である", () => {
      test("formSubmitting が true でない", async () => {
        const mockSetFormTouchedObject = jest.fn();
        const mockSetFormSubmitting = jest.fn();
        const mockSubmit = jest.fn();
        const mockError = jest.fn();
        const mockBeforeValidation = jest.fn();
        const hook = renderHook(() =>
          useOnSubmit(
            { a: "a", b: "b" },
            mockSetFormTouchedObject,
            false,
            mockSetFormSubmitting,
            true
          )
        );

        act(() => {
          const event = {
            preventDefault: () => {
              /*  */
            },
          } as FormEvent<HTMLFormElement>;

          hook.result.current.onSubmit({
            submit: mockSubmit,
            error: mockError,
            beforeValidation: mockBeforeValidation,
          })(event);
        });

        // hook.result.current.onSubmit 内に非同期処理があるため遅延させる
        await wait(0);

        expect(mockSetFormTouchedObject.mock.calls.length).toBe(0);
        expect(mockSetFormSubmitting.mock.calls.length).toBe(2);
        expect(mockSubmit.mock.calls.length).toBe(1);
        expect(mockError.mock.calls.length).toBe(0);
        expect(mockBeforeValidation.mock.calls.length).toBe(1);
      });

      test("formSubmitting が true である", async () => {
        const mockSetFormTouchedObject = jest.fn();
        const mockSetFormSubmitting = jest.fn();
        const mockSubmit = jest.fn();
        const mockError = jest.fn();
        const mockBeforeValidation = jest.fn();
        const hook = renderHook(() =>
          useOnSubmit(
            { a: "a", b: "b" },
            mockSetFormTouchedObject,
            true,
            mockSetFormSubmitting,
            true
          )
        );

        act(() => {
          const event = {
            preventDefault: () => {
              /*  */
            },
          } as FormEvent<HTMLFormElement>;

          hook.result.current.onSubmit({
            submit: mockSubmit,
            error: mockError,
            beforeValidation: mockBeforeValidation,
          })(event);
        });

        expect(mockSetFormTouchedObject.mock.calls.length).toBe(0);
        expect(mockSetFormSubmitting.mock.calls.length).toBe(0);
        expect(mockSubmit.mock.calls.length).toBe(0);
        expect(mockError.mock.calls.length).toBe(0);
        expect(mockBeforeValidation.mock.calls.length).toBe(1);
      });
    });
  });
});
