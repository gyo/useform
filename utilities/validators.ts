export type Validator<T = string | string[] | number | "", A = undefined> = (
  value: T,
  validatorArguments: A
) => string[];

export const validateSignInEmail: Validator<string> = (value) => {
  if (value === "") {
    return ["メールアドレスを入力してください。"];
  }
  return [];
};

export const validateSignInPassword: Validator<string> = (value) => {
  if (value === "") {
    return ["パスワードを入力してください。"];
  }
  return [];
};

export const validateName: Validator<string> = (value) => {
  if (value === "") {
    return ["氏名を入力してください。"];
  }
  if (Array.from(value).length > 255) {
    return ["氏名は255文字以内で入力してください。"];
  }
  return [];
};

export const validateEmail: Validator<string> = (value) => {
  if (value === "") {
    return ["メールアドレスを入力してください。"];
  }
  if (!value.includes("@")) {
    return ["メールアドレスには@を含めてください。"];
  }
  return [];
};

export const validatePassword: Validator<string> = (value) => {
  if (value === "") {
    return ["パスワードを入力してください。"];
  }
  if (Array.from(value).length < 8) {
    return ["パスワードは8文字以上で入力してください。"];
  }
  return [];
};

export type ValidatePasswordForConfirmationOptions = {
  password: string;
};

export const validatePasswordForConfirmation: Validator<
  string,
  ValidatePasswordForConfirmationOptions
> = (value, validatorArguments) => {
  if (value === "") {
    return ["パスワード（確認用）を入力してください。"];
  }
  if (value !== validatorArguments.password) {
    return ["パスワードとパスワード（確認用）を一致させてください。"];
  }
  return [];
};

export const validateAge: Validator<number | ""> = (value) => {
  if (value === "") {
    return ["年齢を入力してください。"];
  }
  return [];
};

export const validateGender: Validator<string> = (value) => {
  if (value === "") {
    return ["性別を入力してください。"];
  }
  return [];
};

export const validateVegetables: Validator<string[]> = (value) => {
  if (value.length === 0) {
    return ["野菜を選択してください。"];
  }
  return [];
};

export const validateFruits: Validator<string> = (value) => {
  if (value === "") {
    return ["果物を選択してください。"];
  }
  return [];
};

export const validateAvatar: Validator<File[] | null> = (value) => {
  if (value == null || value.length === 0) {
    return ["画像を選択してください。"];
  }
  if (value[0].size > 1024 * 100) {
    return ["100KB までのファイルをアップロードできます。"];
  }
  return [];
};
