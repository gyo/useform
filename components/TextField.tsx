import React, { forwardRef, useMemo } from "react";

export type Props = {
  /**
   * 入力欄のラベル。
   */
  label: string;

  /**
   * ラベルを補足するテキスト。
   */
  helperText?: string;

  /**
   * エラーメッセージ。
   * input 要素の aria-invalid 属性に影響する。
   */
  error?: string;

  /**
   * エラーメッセージの p 要素の id 属性。
   * input 要素の aria-describedby 属性にも設定される。
   */
  errorId?: string;

  /**
   * input 要素の id 属性。
   * label 要素の for 属性にも設定される。
   */
  id?: JSX.IntrinsicElements["input"]["id"];

  /**
   * input 要素の required 属性。
   * 必須アイコンの有無に影響する。
   */
  required?: JSX.IntrinsicElements["input"]["required"];
} & Omit<
  JSX.IntrinsicElements["input"],
  "className" | "aria-describedby" | "aria-invalid" | "children"
>;

/**
 * ラベル、入力欄、エラーメッセージをひとまとめにしたコンポーネント。
 */
export const TextField = forwardRef<HTMLInputElement, Props>(
  (
    { label, helperText, error, errorId, type: propsType, ...elementRestProps },
    ref
  ) => {
    const isInvalid = useMemo(() => {
      return error != null;
    }, [error]);

    const [unmasked, setUnMasked] = React.useState(false);

    const computedType = React.useMemo(() => {
      if (propsType === "password" && unmasked) {
        return "text";
      }
      return propsType;
    }, [propsType, unmasked]);

    const handleTogglePassword = React.useCallback(() => {
      setUnMasked(!unmasked);
    }, [unmasked]);

    return (
      <>
        <div className="text-field">
          <label
            className="text-field__label-area"
            htmlFor={elementRestProps.id}
          >
            <span className="text-field__label">{label}</span>

            {elementRestProps.required ? (
              <span className="text-field__required-icon">必須</span>
            ) : null}

            {helperText != null ? (
              <span className="text-field__description">{helperText}</span>
            ) : null}
          </label>

          <div className="text-field__input-area">
            <input
              {...elementRestProps}
              className="text-field__input"
              type={computedType}
              aria-invalid={isInvalid}
              aria-describedby={errorId}
              ref={ref}
            />

            {propsType === "password" ? (
              <button type="button" onClick={handleTogglePassword}>
                {unmasked ? "パスワードを非表示にする" : "パスワードを表示する"}
              </button>
            ) : null}
          </div>

          {isInvalid ? (
            <p className="text-field__error" id={errorId}>
              {error}
            </p>
          ) : null}
        </div>

        <style jsx>{`
          .text-field {
            display: grid;
            gap: 0.5rem;
          }
          .text-field__label-area {
            display: flex;
            flex-wrap: wrap;
            align-items: end;
            gap: 8px;
          }
          .text-field__label {
            font-weight: 500;
            font-size: 12px;
            line-height: 1.7;
            color: #333;
          }
          .text-field__required-icon {
            padding: 2px 8px;
            border-radius: 9999px;
            background: #f14924;
            font-weight: 700;
            font-size: 10px;
            line-height: 1.4;
            color: #fff;
          }
          .text-field__description {
            font-weight: 400;
            font-size: 12px;
            line-height: 1.7;
            color: #333;
          }
          .text-field__input-area {
            display: flex;
            flex-direction: row;
          }
          .text-field__input {
            width: 100%;
            padding: 14px 10px;
            border: 2px solid #f6f6f6;
            border-radius: 10px;
            background: #f6f6f6;
            font-weight: 400;
            font-size: 14px;
            line-height: 1.7;
            color: #828282;
          }
          .text-field__input[aria-invalid="true"] {
            padding: 15px 11px;
            border: 1px solid #fcdbd3;
            background: #fcdbd3;
          }
          .text-field__input:focus-visible {
            outline: none;
            border-color: #f2f2f2;
            color: #333;
          }
          .text-field__input[aria-invalid="true"]:focus-visible {
            border-color: #f8a18d;
          }
          .text-field__error {
            font-weight: 400;
            font-size: 12px;
            line-height: 1.7;
            color: #f14924;
          }
        `}</style>
      </>
    );
  }
);

TextField.displayName = "TextField";
