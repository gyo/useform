import React from "react";
import { useForm } from "../hooks/form";
import {
  validateSignInEmail,
  validateSignInPassword,
} from "../utilities/validators";
import { wait } from "../utilities/wait";
import { Errors } from "./Errors";
import { FormItem } from "./FormItem";
import { FormLabel } from "./FormLabel";

type FormProps = {
  email: string;
  password: string;
};

export const SimpleForm: React.FC = () => {
  const {
    store: { email, password },
    state: { submitting },
    utility: { onChangeText, onBlur, onSubmit },
  } = useForm<FormProps>(
    {
      email: "",
      password: "",
    },
    {
      email: (values) => validateSignInEmail(values.email, undefined),
      password: (values) => validateSignInPassword(values.password, undefined),
    }
  );

  const handleSubmit = React.useMemo(() => {
    return onSubmit({
      submit: async () => {
        console.log("Form is valid.");
        await wait(1000);
        console.log("Form is submitted.");
      },
      error: () => {
        console.log("Form has some error.");
      },
      beforeValidation: () => {
        console.log({ email: email.value, password: password.value });
      },
    });
  }, [email.value, onSubmit, password.value]);

  return (
    <>
      <h2>Simple</h2>
      <form noValidate={true} onSubmit={handleSubmit}>
        <FormItem>
          <FormLabel error={email.hasVisibleError}>メールアドレス</FormLabel>
          <input
            type="email"
            value={email.value}
            onChange={onChangeText("email")}
            onBlur={onBlur("email")}
          />
          {email.hasVisibleError && <Errors errors={email.error} />}
        </FormItem>
        <FormItem>
          <FormLabel error={password.hasVisibleError}>パスワード</FormLabel>
          <input
            type="password"
            value={password.value}
            onChange={onChangeText("password")}
            onBlur={onBlur("password")}
          />
          {password.hasVisibleError && <Errors errors={password.error} />}
        </FormItem>
        <div style={{ marginTop: "1rem" }}>
          <button type="submit">SUBMIT</button>
          {submitting ? "送信中" : ""}
        </div>
      </form>
    </>
  );
};
