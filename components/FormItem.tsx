import React from "react";

export const FormItem: React.FC = (props) => {
  return (
    <div
      style={{
        marginTop: "1rem",
      }}
    >
      {props.children}
    </div>
  );
};
