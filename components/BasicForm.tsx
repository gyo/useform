import React, { useRef } from "react";
import { useForm } from "../hooks/form";
import {
  validateAge,
  validateAvatar,
  validateEmail,
  validateFruits,
  validateGender,
  validateName,
  validatePassword,
  validatePasswordForConfirmation,
  validateVegetables,
} from "../utilities/validators";
import { wait } from "../utilities/wait";
import { Errors } from "./Errors";
import { FormItem } from "./FormItem";
import { FormLabel } from "./FormLabel";

type FormProps = {
  name: string;
  email: string;
  password: string;
  passwordForConfirmation: string;
  age: number | "";
  gender: string;
  comment: string;
  vegetables: string[];
  fruits: string;
  avatar: File[] | null;
};

export const BasicForm: React.FC = () => {
  const {
    store: {
      name,
      email,
      password,
      passwordForConfirmation,
      age,
      gender,
      comment,
      vegetables,
      fruits,
      avatar,
    },
    state: { submitting },
    utility: {
      isChecked,
      onChangeText,
      onChangeNumber,
      onChangeSelect,
      onChangeCheckbox,
      onChangeRadio,
      onChangeFile,
      onBlur,
      onSubmit,
      refresh,
    },
  } = useForm<FormProps>(
    {
      name: "",
      email: "",
      password: "",
      passwordForConfirmation: "",
      age: "",
      gender: "",
      comment: "",
      vegetables: [],
      fruits: "",
      avatar: null,
    },
    {
      name: (values) => validateName(values.name, undefined),
      email: (values) => validateEmail(values.email, undefined),
      password: (values) => validatePassword(values.password, undefined),
      passwordForConfirmation: (values) =>
        validatePasswordForConfirmation(values.passwordForConfirmation, {
          password: values.password,
        }),
      age: (values) => validateAge(values.age, undefined),
      gender: (values) => validateGender(values.gender, undefined),
      vegetables: (values) => validateVegetables(values.vegetables, undefined),
      fruits: (values) => validateFruits(values.fruits, undefined),
      avatar: (values) => validateAvatar(values.avatar, undefined),
    },
    {
      password: ["password", "passwordForConfirmation"],
    }
  );

  const handleSubmit = React.useMemo(() => {
    return onSubmit({
      submit: async () => {
        console.log("Form is valid.");
        await wait(1000);
        console.log("Form is submitted.");
      },
      error: () => {
        console.log("Form has some error.");
      },
      beforeValidation: () => {
        console.log({
          name: name.value,
          email: email.value,
          password: password.value,
          passwordForConfirmation: passwordForConfirmation.value,
          age: age.value,
          gender: gender.value,
          comment: comment.value,
          vegetables: vegetables.value,
          fruits: fruits.value,
          avatar: avatar.value,
        });
      },
    });
  }, [
    age.value,
    avatar.value,
    comment.value,
    email.value,
    fruits.value,
    gender.value,
    name.value,
    onSubmit,
    password.value,
    passwordForConfirmation.value,
    vegetables.value,
  ]);

  const fileInputRef = useRef<HTMLInputElement>(null);

  return (
    <>
      <form noValidate={true} onSubmit={handleSubmit}>
        <h2>Basic</h2>
        <FormItem>
          <label>
            <FormLabel error={name.hasVisibleError}>氏名</FormLabel>
            <input
              value={name.value}
              onChange={onChangeText("name")}
              onBlur={onBlur("name")}
            />
          </label>
          {name.hasVisibleError && <Errors errors={name.error} />}
        </FormItem>
        <FormItem>
          <label>
            <FormLabel error={email.hasVisibleError}>メールアドレス</FormLabel>
            <input
              type="email"
              value={email.value}
              onChange={onChangeText("email")}
              onBlur={onBlur("email")}
            />
          </label>
          {email.hasVisibleError && <Errors errors={email.error} />}
        </FormItem>
        <FormItem>
          <label>
            <FormLabel error={password.hasVisibleError}>パスワード</FormLabel>
            <input
              type="password"
              value={password.value}
              onChange={onChangeText("password")}
              onBlur={onBlur("password")}
            />
          </label>
          {password.hasVisibleError && <Errors errors={password.error} />}
        </FormItem>
        <FormItem>
          <label>
            <FormLabel error={passwordForConfirmation.hasVisibleError}>
              パスワード（確認用）
            </FormLabel>
            <input
              type="password"
              value={passwordForConfirmation.value}
              onChange={onChangeText("passwordForConfirmation")}
              onBlur={onBlur("passwordForConfirmation")}
            />
          </label>
          {passwordForConfirmation.hasVisibleError && (
            <Errors errors={passwordForConfirmation.error} />
          )}
        </FormItem>
        <FormItem>
          <label>
            <FormLabel error={age.hasVisibleError}>年齢</FormLabel>
            <input
              type="number"
              value={age.value}
              onChange={onChangeNumber("age")}
              onBlur={onBlur("age")}
            />
          </label>
          {age.hasVisibleError && <Errors errors={age.error} />}
        </FormItem>
        <FormItem>
          <label>
            <FormLabel error={gender.hasVisibleError}>性別</FormLabel>
            <select value={gender.value} onChange={onChangeSelect("gender")}>
              <option value=""></option>
              <option value="male">男</option>
              <option value="female">女</option>
            </select>
          </label>
          {gender.hasVisibleError && <Errors errors={gender.error} />}
        </FormItem>
        <FormItem>
          <label>
            <FormLabel error={comment.hasVisibleError}>コメント</FormLabel>
            <textarea
              value={comment.value}
              onChange={onChangeText("comment")}
              onBlur={onBlur("comment")}
            />
          </label>
          {comment.hasVisibleError && <Errors errors={comment.error} />}
        </FormItem>
        <FormItem>
          <FormLabel error={vegetables.hasVisibleError}>野菜</FormLabel>
          <div>
            <label>
              <span>にんじん</span>
              <input
                type="checkbox"
                value="carrot"
                checked={isChecked("vegetables", "carrot")}
                onChange={onChangeCheckbox("vegetables")}
                onBlur={onBlur("vegetables")}
              />
            </label>
          </div>
          <div>
            <label>
              <span>玉ねぎ</span>
              <input
                type="checkbox"
                value="onion"
                checked={isChecked("vegetables", "onion")}
                onChange={onChangeCheckbox("vegetables")}
                onBlur={onBlur("vegetables")}
              />
            </label>
          </div>
          <div>
            <label>
              <span>じゃがいも</span>
              <input
                type="checkbox"
                value="potato"
                checked={isChecked("vegetables", "potato")}
                onChange={onChangeCheckbox("vegetables")}
                onBlur={onBlur("vegetables")}
              />
            </label>
          </div>
          {vegetables.hasVisibleError && <Errors errors={vegetables.error} />}
        </FormItem>
        <FormItem>
          <FormLabel error={fruits.hasVisibleError}>果物</FormLabel>
          <div>
            <label>
              <span>りんご</span>
              <input
                type="radio"
                value="apple"
                checked={isChecked("fruits", "apple")}
                onChange={onChangeRadio("fruits")}
                onBlur={onBlur("fruits")}
              />
            </label>
          </div>
          <div>
            <label>
              <span>バナナ</span>
              <input
                type="radio"
                value="banana"
                checked={isChecked("fruits", "banana")}
                onChange={onChangeRadio("fruits")}
                onBlur={onBlur("fruits")}
              />
            </label>
          </div>
          <div>
            <label>
              <span>さくらんぼ</span>
              <input
                type="radio"
                value="cherry"
                checked={isChecked("fruits", "cherry")}
                onChange={onChangeRadio("fruits")}
                onBlur={onBlur("fruits")}
              />
            </label>
          </div>
          {fruits.hasVisibleError && <Errors errors={fruits.error} />}
        </FormItem>
        <FormItem>
          <label>
            <FormLabel error={avatar.hasVisibleError}>画像</FormLabel>
            <input
              type="file"
              ref={fileInputRef}
              onChange={onChangeFile("avatar")}
            />
          </label>
          {avatar.hasVisibleError && <Errors errors={avatar.error} />}
        </FormItem>
        <div style={{ marginTop: "1rem" }}>
          <button type="submit">SUBMIT</button>
          {submitting ? "送信中" : ""}
        </div>
        <div style={{ marginTop: "1rem" }}>
          <button
            type="button"
            onClick={() => {
              refresh();
              if (fileInputRef.current != null) {
                fileInputRef.current.value = "";
              }
            }}
          >
            REFRESH
          </button>
        </div>
      </form>
    </>
  );
};
