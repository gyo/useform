import React, { useMemo } from "react";
import { useForm } from "react-hook-form";
import { wait } from "../utilities/wait";
import { Errors } from "./Errors";
import { FormItem } from "./FormItem";
import { FormLabel } from "./FormLabel";
import { MyInput } from "./MyInput";

type FormProps = {
  email: string;
  password: string;
};

export const SimpleForm2: React.FC = () => {
  const {
    register,
    formState: { isSubmitting, errors },
    handleSubmit,
  } = useForm<FormProps>({
    mode: "onTouched",
    defaultValues: {
      email: "",
      password: "",
    },
  });
  const onSubmit = useMemo(() => {
    return handleSubmit(
      async (data) => {
        console.log("Form is valid.");
        console.log({ email: data.email, password: data.password });
        await wait(1000);
        console.log("Form is submitted.");
      },
      () => {
        console.log("Form has some error.");
      }
    );
  }, [handleSubmit]);

  return (
    <>
      <h2>Simple</h2>
      <form noValidate={true} onSubmit={onSubmit}>
        <FormItem>
          <FormLabel error={errors.email != null ? true : false}>
            メールアドレス
          </FormLabel>
          <MyInput
            {...register("email", {
              required: "メールアドレスを入力してください。",
            })}
            type="email"
          />
          {errors.email?.message != null && (
            <Errors errors={[errors.email.message]} />
          )}
        </FormItem>
        <FormItem>
          <FormLabel error={errors.password != null ? true : false}>
            パスワード
          </FormLabel>
          <MyInput
            {...register("password", {
              required: "パスワードを入力してください。",
            })}
            type="password"
          />
          {errors.password?.message != null && (
            <Errors errors={[errors.password?.message]} />
          )}
        </FormItem>
        <div style={{ marginTop: "1rem" }}>
          <button type="submit" disabled={isSubmitting}>
            SUBMIT
          </button>
          {isSubmitting ? "送信中" : ""}
        </div>
      </form>
    </>
  );
};
