import React, { ReactNode } from "react";

type Props = {
  children: ReactNode;
};

export const FormLayout: React.VFC<Props> = ({ children }) => {
  return (
    <>
      <div className="form-layout">{children}</div>

      <style jsx>{`
        .form-layout {
          margin: 0 auto;
          max-width: 768px;
          padding: 2rem;
          display: grid;
          grid-template-columns: 100%;
          gap: 2rem;
        }
      `}</style>
    </>
  );
};
