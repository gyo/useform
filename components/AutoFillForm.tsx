import React from "react";
import { useForm } from "../hooks/form";
import { validateEmail, validatePassword } from "../utilities/validators";
import { Errors } from "./Errors";
import { FormItem } from "./FormItem";
import { FormLabel } from "./FormLabel";

type FormProps = {
  email: string;
  password: string;
};

export const AutoFillForm: React.FC = () => {
  const {
    store: { email, password },
    state: { isValid, submitting },
    utility: {
      setValue,
      setValues,
      onChangeText,
      onBlur,
      beginSubmit,
      endSubmit,
      touchAll,
    },
  } = useForm<FormProps>(
    {
      email: "",
      password: "",
    },
    {
      email: (values) => validateEmail(values.email, undefined),
      password: (values) => validatePassword(values.password, undefined),
    }
  );

  return (
    <>
      <h2>Auto Fill</h2>
      <form
        noValidate={true}
        onSubmit={(e) => {
          e.preventDefault();
          if (!isValid) {
            console.log("Form has some error.");
            touchAll();
          } else {
            console.log("Form is valid.");
            beginSubmit();
            setTimeout(() => {
              endSubmit();
            }, 1000);
          }
        }}
      >
        <button
          type="button"
          onClick={() => {
            setValues({
              email: "sample@sample.com",
              password: "password",
            });
          }}
        >
          自動入力
        </button>
        <FormItem>
          <FormLabel error={email.hasVisibleError}>メールアドレス</FormLabel>
          <input
            type="email"
            value={email.value}
            onChange={onChangeText("email")}
            onBlur={onBlur("email")}
          />
          <button
            type="button"
            onClick={() => {
              setValue("email", "sample@sample.com");
            }}
          >
            自動入力
          </button>
          {email.hasVisibleError && <Errors errors={email.error} />}
        </FormItem>
        <FormItem>
          <FormLabel error={password.hasVisibleError}>パスワード</FormLabel>
          <input
            type="password"
            value={password.value}
            onChange={onChangeText("password")}
            onBlur={onBlur("password")}
          />
          <button
            type="button"
            onClick={() => {
              setValue("password", "password");
            }}
          >
            自動入力
          </button>
          {password.hasVisibleError && <Errors errors={password.error} />}
        </FormItem>
        <div style={{ marginTop: "1rem" }}>
          <button type="submit">SUBMIT</button>
          {submitting ? "送信中" : ""}
        </div>
      </form>
    </>
  );
};
