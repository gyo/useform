import React, { forwardRef } from "react";

type Props = JSX.IntrinsicElements["input"];

export const MyInput = forwardRef<HTMLInputElement, Props>((props, ref) => {
  return <input {...props} ref={ref} />;
});
MyInput.displayName = "MyInput";
