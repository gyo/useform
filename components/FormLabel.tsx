import React from "react";

interface Props {
  error: boolean;
}

export const FormLabel: React.FC<Props> = (props) => {
  return (
    <div
      style={{
        fontWeight: "bold",
        color: props.error ? "crimson" : "black",
      }}
    >
      {props.children}
    </div>
  );
};
