import React, { useMemo } from "react";
import { useForm } from "react-hook-form";
import { wait } from "../utilities/wait";
import { Errors } from "./Errors";
import { FormItem } from "./FormItem";
import { FormLabel } from "./FormLabel";
import { MyInput } from "./MyInput";

type FormProps = {
  email: string;
  password: string;
};

export const AutoFillForm2: React.FC = () => {
  const {
    register,
    formState: { isSubmitting, errors },
    handleSubmit,
    setValue,
  } = useForm<FormProps>({
    mode: "onTouched",
    defaultValues: {
      email: "",
      password: "",
    },
  });

  const onSubmit = useMemo(() => {
    return handleSubmit(
      async (data) => {
        console.log("Form is valid.");
        console.log({ email: data.email, password: data.password });
        await wait(1000);
        console.log("Form is submitted.");
      },
      () => {
        console.log("Form has some error.");
      }
    );
  }, [handleSubmit]);

  return (
    <>
      <h2>Auto Fill</h2>
      <form noValidate={true} onSubmit={onSubmit}>
        <button
          type="button"
          onClick={() => {
            setValue("email", "sample@sample.com");
            setValue("password", "password");
          }}
        >
          自動入力
        </button>
        <FormItem>
          <FormLabel error={errors.email != null ? true : false}>
            メールアドレス
          </FormLabel>
          <MyInput
            {...register("email", {
              required: "メールアドレスを入力してください。",
              pattern: {
                value: /@/,
                message: "メールアドレスには@を含めてください。",
              },
            })}
            type="email"
          />
          <button
            type="button"
            onClick={() => {
              setValue("email", "sample@sample.com");
            }}
          >
            自動入力
          </button>
          {errors.email?.message != null && (
            <Errors errors={[errors.email.message]} />
          )}
        </FormItem>
        <FormItem>
          <FormLabel error={errors.password != null ? true : false}>
            パスワード
          </FormLabel>
          <MyInput
            {...register("password", {
              required: "パスワードを入力してください。",
              minLength: {
                value: 8,
                message: "パスワードは8文字以上で入力してください。",
              },
            })}
            type="password"
          />
          <button
            type="button"
            onClick={() => {
              setValue("password", "password");
            }}
          >
            自動入力
          </button>
          {errors.password?.message != null && (
            <Errors errors={[errors.password.message]} />
          )}
        </FormItem>
        <div style={{ marginTop: "1rem" }}>
          <button type="submit" disabled={isSubmitting}>
            SUBMIT
          </button>
          {isSubmitting ? "送信中" : ""}
        </div>
      </form>
    </>
  );
};
