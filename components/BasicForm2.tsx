import React, { useEffect, useMemo, useRef } from "react";
import { NestedValue, useForm } from "react-hook-form";
import { wait } from "../utilities/wait";
import { Errors } from "./Errors";
import { FormItem } from "./FormItem";
import { FormLabel } from "./FormLabel";
import { MyInput } from "./MyInput";
import { MySelect } from "./MySelect";

type FormProps = {
  name: string;
  email: string;
  password: string;
  passwordForConfirmation: string;
  age: number;
  gender: string;
  comment: string;
  vegetables: NestedValue<string[]>;
  fruits: string;
  avatar: FileList | null;
};

export const BasicForm2: React.FC = () => {
  const {
    register,
    formState: { touchedFields, isSubmitting, errors },
    handleSubmit,
    reset,
    watch,
    getValues,
    trigger,
  } = useForm<FormProps>({
    mode: "onTouched",
    defaultValues: {
      name: "",
      email: "",
      password: "",
      passwordForConfirmation: "",
      age: NaN,
      gender: "",
      comment: "",
      vegetables: [],
      fruits: "",
      avatar: null,
    },
  });
  const onSubmit = useMemo(() => {
    return handleSubmit(
      async (data) => {
        console.log("Form is valid.");
        console.log({
          name: data.name,
          email: data.email,
          password: data.password,
          passwordForConfirmation: data.passwordForConfirmation,
          age: data.age,
          gender: data.gender,
          comment: data.comment,
          vegetables: data.vegetables,
          fruits: data.fruits,
          avatar: data.avatar,
        });
        await wait(1000);
        console.log("Form is submitted.");
      },
      (errors) => {
        console.log("Form has some error.");
        console.log(errors);
      }
    );
  }, [handleSubmit]);

  const password = watch("password");

  const isFirstRender = useRef(true);

  useEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false;
      return;
    }

    if (touchedFields.passwordForConfirmation !== true) {
      return;
    }

    trigger("passwordForConfirmation");
  }, [touchedFields, touchedFields.passwordForConfirmation, trigger, password]);

  return (
    <>
      <form noValidate={true} onSubmit={onSubmit}>
        <h2>Basic</h2>
        <FormItem>
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
          <label>
            <FormLabel error={errors.name != null ? true : false}>
              氏名
            </FormLabel>
            <MyInput
              {...register("name", {
                required: "氏名を入力してください。",
                maxLength: {
                  value: 255,
                  message: "氏名は255文字以内で入力してください。",
                },
              })}
            />
          </label>
          {errors.name?.message != null && (
            <Errors errors={[errors.name.message]} />
          )}
        </FormItem>
        <FormItem>
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
          <label>
            <FormLabel error={errors.email != null ? true : false}>
              メールアドレス
            </FormLabel>
            <MyInput
              {...register("email", {
                required: "メールアドレスを入力してください。",
                pattern: {
                  value: /@/,
                  message: "メールアドレスには@を含めてください。",
                },
              })}
              type="email"
            />
          </label>
          {errors.email?.message != null && (
            <Errors errors={[errors.email.message]} />
          )}
        </FormItem>
        <FormItem>
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
          <label>
            <FormLabel error={errors.password != null ? true : false}>
              パスワード
            </FormLabel>
            <MyInput
              {...register("password", {
                required: "パスワードを入力してください。",
                minLength: {
                  value: 8,
                  message: "パスワードは8文字以上で入力してください。",
                },
              })}
              type="password"
            />
          </label>
          {errors.password?.message != null && (
            <Errors errors={[errors.password.message]} />
          )}
        </FormItem>
        <FormItem>
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
          <label>
            <FormLabel
              error={errors.passwordForConfirmation != null ? true : false}
            >
              パスワード（確認用）
            </FormLabel>
            <MyInput
              {...register("passwordForConfirmation", {
                required: "パスワード（確認用）を入力してください。",
                validate: (value) => {
                  if (value !== getValues("password")) {
                    return "パスワードとパスワード（確認用）を一致させてください。";
                  }
                },
              })}
              type="password"
            />
          </label>
          {errors.passwordForConfirmation?.message != null && (
            <Errors errors={[errors.passwordForConfirmation.message]} />
          )}
        </FormItem>
        <FormItem>
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
          <label>
            <FormLabel error={errors.age != null ? true : false}>
              年齢
            </FormLabel>
            <MyInput
              {...register("age", {
                required: "年齢を入力してください。",
                valueAsNumber: true,
              })}
              type="number"
            />
          </label>
          {errors.age?.message != null && (
            <Errors errors={[errors.age.message]} />
          )}
        </FormItem>
        <FormItem>
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
          <label>
            <FormLabel error={errors.gender != null ? true : false}>
              性別
            </FormLabel>
            <MySelect
              {...register("gender", {
                required: "性別を入力してください。",
              })}
              options={[
                { value: "", label: "" },
                { value: "male", label: "男" },
                { value: "female", label: "女" },
              ]}
            />
          </label>
          {errors.gender?.message != null && (
            <Errors errors={[errors.gender.message]} />
          )}
        </FormItem>
        <FormItem>
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
          <label>
            <FormLabel error={errors.comment != null ? true : false}>
              コメント
            </FormLabel>
            <textarea {...register("comment")} />
          </label>
          {errors.comment?.message != null && (
            <Errors errors={[errors.comment.message]} />
          )}
        </FormItem>
        <FormItem>
          <FormLabel error={errors.vegetables != null ? true : false}>
            野菜
          </FormLabel>
          <div>
            {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
            <label>
              <span>にんじん</span>
              <MyInput
                {...register("vegetables", {
                  required: "野菜を選択してください。",
                })}
                type="checkbox"
                value="carrot"
              />
            </label>
          </div>
          <div>
            {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
            <label>
              <span>玉ねぎ</span>
              <MyInput
                {...register("vegetables", {
                  required: "野菜を選択してください。",
                })}
                type="checkbox"
                value="onion"
              />
            </label>
          </div>
          <div>
            {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
            <label>
              <span>じゃがいも</span>
              <MyInput
                {...register("vegetables", {
                  required: "野菜を選択してください。",
                })}
                type="checkbox"
                value="potato"
              />
            </label>
          </div>
          {errors.vegetables?.message != null && (
            <Errors errors={[errors.vegetables.message]} />
          )}
        </FormItem>
        <FormItem>
          <FormLabel error={errors.fruits != null ? true : false}>
            果物
          </FormLabel>
          <div>
            {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
            <label>
              <span>りんご</span>
              <MyInput
                {...register("fruits", {
                  required: "果物を選択してください。",
                })}
                type="radio"
                value="apple"
              />
            </label>
          </div>
          <div>
            {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
            <label>
              <span>バナナ</span>
              <MyInput
                {...register("fruits", {
                  required: "果物を選択してください。",
                })}
                type="radio"
                value="banana"
              />
            </label>
          </div>
          <div>
            {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
            <label>
              <span>さくらんぼ</span>
              <MyInput
                {...register("fruits", {
                  required: "果物を選択してください。",
                })}
                type="radio"
                value="cherry"
              />
            </label>
          </div>
          {errors.fruits?.message != null && (
            <Errors errors={[errors.fruits.message]} />
          )}
        </FormItem>
        <FormItem>
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control -- MyInput 内に input 要素が存在するため */}
          <label>
            <FormLabel error={errors.avatar != null ? true : false}>
              画像
            </FormLabel>
            <MyInput
              {...register("avatar", {
                validate: (value) => {
                  if (value == null || value.length === 0) {
                    return "画像を選択してください。";
                  }
                  if (value[0].size > 1024 * 100) {
                    return "100KB までのファイルをアップロードできます。";
                  }
                },
              })}
              type="file"
            />
          </label>
          {errors.avatar?.message != null && (
            <Errors errors={[errors.avatar.message]} />
          )}
        </FormItem>
        <div style={{ marginTop: "1rem" }}>
          <button type="submit" disabled={isSubmitting}>
            SUBMIT
          </button>
          {isSubmitting ? "送信中" : ""}
        </div>
        <div style={{ marginTop: "1rem" }}>
          <button
            type="button"
            onClick={() => {
              reset();
            }}
          >
            REFRESH
          </button>
        </div>
      </form>
    </>
  );
};
