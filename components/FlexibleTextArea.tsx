import React, { forwardRef, useEffect, useRef, useState } from "react";

type Props = {
  minRows?: number;
  maxRows?: number;
} & Omit<JSX.IntrinsicElements["textarea"], "className" | "children">;

export const FlexibleTextArea = forwardRef<HTMLTextAreaElement, Props>(
  ({ minRows, maxRows, ...elementProps }, ref) => {
    // ダミー要素の高さをとるため、textarea 要素の入力値を React でも管理する
    const [value, setValue] = useState(elementProps.value);

    // textarea 要素にイベントリスナを設定するために DOM への参照を保持する
    const selfElementRef = useRef<HTMLDivElement>(null);

    // 初期化時にダミー要素の高さをとる
    // textarea 要素に、ダミー要素の高さをとるためのイベントリスナを設定する
    useEffect(() => {
      const textAreaElement = selfElementRef.current?.querySelector("textarea");

      const updateValue = (target: HTMLTextAreaElement) => {
        const value = target.value ?? "";
        setValue(value + "\u200b");
      };

      const handleChange = (e: Event) => {
        const target = e.target;
        if (target instanceof HTMLTextAreaElement) {
          updateValue(target);
        }
      };

      if (textAreaElement != null) {
        updateValue(textAreaElement);
      }

      textAreaElement?.addEventListener("input", handleChange);

      return () => {
        textAreaElement?.removeEventListener("input", handleChange);
      };
    }, []);

    return (
      <>
        <div className="flexible-text-area" ref={selfElementRef}>
          <div className="flexible-text-area__dummy" aria-hidden="true">
            {value}
          </div>

          <textarea
            {...elementProps}
            className="flexible-text-area__input"
            ref={ref}
          />
        </div>

        <style jsx>{`
          .flexible-text-area {
            position: relative;
          }
          .flexible-text-area__dummy {
            display: block;
            padding: 10px 12px;
            min-height: ${
              minRows != null
                ? `calc(10px * 2 + 14px * 1.7 * ${minRows})` // padding * 2 + font-size * line-height * rows
                : "none"
            };
            max-height: ${
              maxRows != null
                ? `calc(10px * 2 + 14px * 1.7 * ${maxRows})` // padding * 2 + font-size * line-height * rows
                : "none"
            };
            font-weight: 400;
            font-size: 14px;
            line-height: 1.7;
            white-space: pre-wrap;
            overflow-wrap: break-word;
            visibility: hidden;
          }
          .flexible-text-area__input {
            position: absolute;
            top: 0;
            left: 0;
            display: block;
            border: 2px solid f6f6f6;
            border-radius: 10px;
            padding: 8px 10px;
            width: 100%;
            height: 100%;
            background: #f6f6f6;
            font-weight: 400;
            font-size: 14px;
            line-height: 1.7;
            color: #828282;
            resize: none;
          }
          .flexible-text-area__input[aria-invalid="true"] {
            padding: 9px 11px;
            border-width: 1px
            border-color: #fcdbd3;
            background: #fcdbd3;
          }
          .flexible-text-area__input:focus-visible {
            outline: none;
            border-color: #f2f2f2;
            color: #333;
          }
          .flexible-text-area__input[aria-invalid="true"]:focus-visible {
            border-color: #f8a18d;
          }
        `}</style>
      </>
    );
  }
);

FlexibleTextArea.displayName = "FlexibleTextArea";
