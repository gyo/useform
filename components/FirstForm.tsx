import React from "react";
import { useForm } from "../hooks/form";
import { FormItem } from "./FormItem";
import { FormLabel } from "./FormLabel";
import { MyInput } from "./MyInput";

type FormProps = {
  text: string;
};

export const FirstForm: React.FC = () => {
  const {
    store: { text },
    state: { submitting },
    utility: { onChangeText, beginSubmit, endSubmit },
  } = useForm<FormProps>({
    text: "",
  });

  return (
    <>
      <h2>First</h2>
      <form
        noValidate={true}
        onSubmit={(e) => {
          e.preventDefault();
          console.log({ text: text.value });
          beginSubmit();
          setTimeout(() => {
            endSubmit();
          }, 1000);
        }}
      >
        <FormItem>
          <FormLabel error={false}>テキスト</FormLabel>
          <MyInput value={text.value} onChange={onChangeText("text")} />
        </FormItem>
        <div style={{ marginTop: "1rem" }}>
          <button type="submit">SUBMIT</button>
          {submitting ? "送信中" : ""}
        </div>
      </form>
    </>
  );
};
