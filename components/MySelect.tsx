import React, { forwardRef } from "react";

type Props = Omit<JSX.IntrinsicElements["select"], "children"> & {
  options: { value: string; label: string }[];
};

export const MySelect = forwardRef<HTMLSelectElement, Props>(
  ({ options, ...selectProps }, ref) => {
    return (
      <select {...selectProps} ref={ref}>
        {options.map((option) => {
          return (
            <option key={option.value} value={option.value}>
              {option.label}
            </option>
          );
        })}
      </select>
    );
  }
);
MySelect.displayName = "MySelect";
