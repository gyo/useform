import React, { forwardRef, useMemo } from "react";
import { FlexibleTextArea } from "../components/FlexibleTextArea";

type Props = {
  /**
   * 入力欄のラベル。
   */
  label: string;

  /**
   * ラベルを補足するテキスト。
   */
  helperText?: string;

  /**
   * エラーメッセージ。
   * textarea 要素の aria-invalid 属性に影響する。
   */
  error?: string;

  /**
   * エラーメッセージの p 要素の id 属性。
   * textarea 要素の aria-describedby 属性にも設定される。
   */
  errorId?: string;

  /**
   * textarea 要素の id 属性。
   * label 要素の for 属性にも設定される。
   */
  id?: JSX.IntrinsicElements["textarea"]["id"];

  /**
   * textarea 要素の required 属性。
   * 必須アイコンの有無に影響する。
   */
  required?: JSX.IntrinsicElements["textarea"]["required"];

  minRows?: number;
  maxRows?: number;
} & Omit<
  JSX.IntrinsicElements["textarea"],
  "className" | "aria-describedby" | "aria-invalid" | "children"
>;

/**
 * ラベル、入力欄、エラーメッセージをひとまとめにしたコンポーネント。
 */
export const TextArea = forwardRef<HTMLTextAreaElement, Props>(
  (
    {
      label,
      helperText,
      error,
      errorId,
      minRows = 5,
      maxRows = 10,
      ...elementRestProps
    },
    ref
  ) => {
    const isInvalid = useMemo(() => {
      return error != null;
    }, [error]);

    return (
      <>
        <div className="text-field">
          <label
            className="text-field__label-area"
            htmlFor={elementRestProps.id}
          >
            <span className="text-field__label">{label}</span>

            {elementRestProps.required ? (
              <span className="text-field__required-icon">必須</span>
            ) : null}

            {helperText != null ? (
              <span className="text-field__description">{helperText}</span>
            ) : null}
          </label>

          <div className="text-field__input-area">
            <FlexibleTextArea
              {...elementRestProps}
              minRows={minRows}
              maxRows={maxRows}
              aria-invalid={isInvalid}
              aria-describedby={errorId}
              ref={ref}
            />
          </div>

          {isInvalid ? (
            <p className="text-field__error" id={errorId}>
              {error}
            </p>
          ) : null}
        </div>

        <style jsx>{`
          .text-field {
            display: grid;
            grid-template-columns: 100%;
            gap: 0.5rem;
          }
          .text-field__label-area {
            display: flex;
            flex-wrap: wrap;
            align-items: end;
            gap: 8px;
          }
          .text-field__label {
            font-weight: 500;
            font-size: 12px;
            line-height: 1.7;
            color: #333;
          }
          .text-field__required-icon {
            padding: 2px 8px;
            border-radius: 9999px;
            background: #f14924;
            font-weight: 700;
            font-size: 10px;
            line-height: 1.4;
            color: #fff;
          }
          .text-field__description {
            font-weight: 400;
            font-size: 12px;
            line-height: 1.7;
            color: #333;
          }
          .text-field__input-area {
          }
          .text-field__error {
            font-weight: 400;
            font-size: 12px;
            line-height: 1.7;
            color: #f14924;
          }
        `}</style>
      </>
    );
  }
);

TextArea.displayName = "TextArea";
