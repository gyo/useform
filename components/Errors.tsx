import React from "react";

interface Props {
  errors: string[];
}

export const Errors: React.FC<Props> = (props) => {
  return (
    <ul style={{ listStyle: "none", margin: 0, padding: 0 }}>
      {props.errors.map((error, index) => (
        <li key={index}>
          <small style={{ color: "crimson" }}>{error}</small>
        </li>
      ))}
    </ul>
  );
};
