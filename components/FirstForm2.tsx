import React, { useMemo } from "react";
import { useForm } from "react-hook-form";
import { wait } from "../utilities/wait";
import { FormItem } from "./FormItem";
import { FormLabel } from "./FormLabel";
import { MyInput } from "./MyInput";

type FormProps = {
  text: string;
};

export const FirstForm2: React.FC = () => {
  const {
    register,
    formState: { isSubmitting },
    handleSubmit,
  } = useForm<FormProps>({
    mode: "onTouched",
    defaultValues: {
      text: "",
    },
  });
  const onSubmit = useMemo(() => {
    return handleSubmit(async (data) => {
      console.log("Form is valid.");
      console.log({ text: data.text });
      await wait(1000);
      console.log("Form is submitted.");
    });
  }, [handleSubmit]);

  return (
    <>
      <h2>First</h2>
      <form noValidate={true} onSubmit={onSubmit}>
        <FormItem>
          <FormLabel error={false}>テキスト</FormLabel>
          <MyInput {...register("text")} />
        </FormItem>
        <div style={{ marginTop: "1rem" }}>
          <button type="submit" disabled={isSubmitting}>
            SUBMIT
          </button>
          {isSubmitting ? "送信中" : ""}
        </div>
      </form>
    </>
  );
};
