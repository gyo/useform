import { NextPage } from "next";
import React from "react";
import { AutoFillForm } from "../components/AutoFillForm";
import { BasicForm } from "../components/BasicForm";
import { FilledForm } from "../components/FilledForm";
import { FirstForm } from "../components/FirstForm";
import { SimpleForm } from "../components/SimpleForm";

const Page: NextPage = () => {
  return (
    <>
      <h1>Form Sample</h1>
      <FirstForm />
      <SimpleForm />
      <BasicForm />
      <FilledForm />
      <AutoFillForm />
    </>
  );
};

export default Page;
