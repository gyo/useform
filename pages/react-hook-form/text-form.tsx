import { NextPage } from "next";
import NextHead from "next/head";
import React from "react";
import { useForm } from "react-hook-form";
import { FormLayout } from "../../components/FormLayout";
import { TextArea } from "../../components/TextArea";
import { TextField } from "../../components/TextField";

const Page: NextPage = () => {
  const {
    register,
    formState: { errors },
    handleSubmit,
  } = useForm({
    mode: "onTouched",
    defaultValues: {
      text1: "",
      text2: "",
      text3: "",
      text4: "",
      text5: "テキスト5",
      email: "",
      number: "",
      password: "",
      multiline1: "",
      multiline2: "",
      multiline3: "複数行\nテキスト\n3",
      multiline4: "",
    },
  });

  return (
    <>
      <NextHead>
        <link rel="stylesheet" href="/ress.css" />
      </NextHead>

      <h1>Form Sample</h1>

      <form
        onSubmit={handleSubmit((data) => {
          console.log(data);
        })}
        noValidate={true}
      >
        <FormLayout>
          <TextField
            {...register("text1")}
            id="text1"
            label="バリデーションなし"
            error={errors.text1?.message}
          />

          <TextField
            {...register("text2", {
              required: "入力してください。",
            })}
            id="text2"
            label="入力必須"
            required={true}
            error={errors.text2?.message}
          />

          <TextField
            {...register("text3", {
              minLength: {
                value: 8,
                message: "8文字以上で入力してください。",
              },
            })}
            id="text3"
            label="8文字以上"
            helperText="8文字以上で入力してください。"
            error={errors.text3?.message}
          />

          <TextField
            {...register("text4", {
              required: "入力してください。",
              minLength: {
                value: 8,
                message: "8文字以上で入力してください。",
              },
            })}
            id="text4"
            label="入力必須、8文字以上"
            required={true}
            helperText="8文字以上で入力してください。"
            error={errors.text4?.message}
          />

          <TextField
            {...register("text5", {
              required: "入力してください。",
            })}
            id="text5"
            label="初期値あり"
            required={true}
            error={errors.text5?.message}
          />

          <TextField
            {...register("email")}
            id="email"
            label="メールアドレス"
            type="email"
            error={errors.email?.message}
          />

          <TextField
            {...register("number")}
            id="number"
            label="数値"
            type="number"
            error={errors.number?.message}
          />

          <TextField
            {...register("password")}
            id="password"
            label="パスワード"
            type="password"
            error={errors.password?.message}
          />

          <TextArea
            {...register("multiline1")}
            id="multiline1"
            label="複数行、バリデーションなし"
            error={errors.multiline1?.message}
          />

          <TextArea
            {...register("multiline2", {
              required: "入力してください。",
              minLength: {
                value: 16,
                message: "16文字以上で入力してください。",
              },
            })}
            id="multiline2"
            label="複数行、入力必須、16文字以上"
            required={true}
            helperText="16文字以上で入力してください。"
            error={errors.multiline2?.message}
          />

          <TextArea
            {...register("multiline3", {
              maxLength: {
                value: 100,
                message: "100文字以内で入力してください。",
              },
            })}
            id="multiline3"
            label="初期値あり"
            helperText="100文字以内で入力してください。"
            error={errors.multiline3?.message}
            minRows={2}
            maxRows={5}
          />

          <button type="submit">送信</button>

          <div style={{ height: "999px" }}></div>
        </FormLayout>
      </form>
    </>
  );
};

export default Page;
