import { NextPage } from "next";
import React from "react";
import { AutoFillForm2 } from "../components/AutoFillForm2";
import { BasicForm2 } from "../components/BasicForm2";
import { FilledForm2 } from "../components/FilledForm2";
import { FirstForm2 } from "../components/FirstForm2";
import { SimpleForm2 } from "../components/SimpleForm2";

const Page: NextPage = () => {
  return (
    <>
      <h1>Form Sample</h1>
      <FirstForm2 />
      <SimpleForm2 />
      <BasicForm2 />
      <FilledForm2 />
      <AutoFillForm2 />
    </>
  );
};

export default Page;
